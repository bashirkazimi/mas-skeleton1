/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.behaviour.ruralagentcoordinator;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.RACoordinator;
import cat.urv.imas.agent.UtilsAgents;
import cat.urv.imas.onthology.Injured;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import java.io.IOException;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bashir
 */
public class WaitForInfoFromRuralAgentsBehaviour extends OneShotBehaviour {

    @Override
    public void action() {
        RACoordinator RACoord = (RACoordinator) myAgent;
        LinkedList<Injured> result = new LinkedList();
        RACoord.log("Waiting for results...");
        for (AID ruralAgentAID : RACoord.getRAList()){
            // Message bid template
            MessageTemplate msbt = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
                                                        MessageTemplate.MatchSender(ruralAgentAID));
            
            ACLMessage msg = myAgent.blockingReceive(msbt);
            RACoord.log("Received from "+ruralAgentAID.getLocalName());
            try {
                Object obj = (Object) msg.getContentObject();
                result.addAll((LinkedList<Injured>) obj);
            } catch (UnreadableException ex) {
                Logger.getLogger(WaitForInfoFromRuralAgentsBehaviour.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        RACoord.log("tortuga RAs: "+result.toString());
        
        //  Search for Coordinator Agent
        ServiceDescription searchCriterionCoordinatorAgent = new ServiceDescription();
        searchCriterionCoordinatorAgent.setType(AgentType.COORDINATOR.toString());
        
        AID coordAgent = UtilsAgents.searchAgent(RACoord, searchCriterionCoordinatorAgent);
        
        ACLMessage data = new ACLMessage(ACLMessage.INFORM);
        data.addReceiver(coordAgent);
        try {
            data.setContentObject((Serializable) result);
            RACoord.log("Data sent to COORDINATOR after info from rural: "+result.toString());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        RACoord.send(data);
        
        RACoord.addBehaviour(new ReceiveInjuredListBehaviour());
    }
    
}
