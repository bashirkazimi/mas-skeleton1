/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.behaviour.ruralagentcoordinator;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.HCoordinator;
import cat.urv.imas.agent.RACoordinator;
import cat.urv.imas.agent.UtilsAgents;
import cat.urv.imas.onthology.GameSettings;
import cat.urv.imas.behaviour.ruralagentcoordinator.RunContractNetInitiator;
import cat.urv.imas.onthology.Injured;
import cat.urv.imas.onthology.MessageContent;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Andres
 */
public class ReceiveInjuredListBehaviour extends OneShotBehaviour{

    @Override
    public void action() {
        
        RACoordinator RACoord = (RACoordinator) myAgent;
        // Ask to the Coord Agent for the ligthly injured people
        
        //  Search for Coordinator Agent
        ServiceDescription searchCriterionCoordinatorAgent = new ServiceDescription();
        searchCriterionCoordinatorAgent.setType(AgentType.COORDINATOR.toString());
        
        AID coordAgent = UtilsAgents.searchAgent(RACoord, searchCriterionCoordinatorAgent);
        
        // Waiting for CoordinatorAgent to send list of injured people
        MessageTemplate mstListInjured = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.REQUEST),
                                                             MessageTemplate.MatchSender(coordAgent));
        
        // Blocked waiting for message from Coordinator Agent with the list of injured people
        ACLMessage msg = myAgent.blockingReceive(mstListInjured);
        ACLMessage reply = msg.createReply();
        
        // Once received, obtained content of the message
        try{
            Object content = msg.getContentObject();
            // add if content null?
            GameSettings game = (GameSettings) content;
            RACoord.log("Got the game ");
         
             List<Injured> injuredPeople = game.getInjuredPeopleList();
            ArrayList<Injured> lightlyInjuredList = new ArrayList<Injured>();
            
            // Select the severly injured people from the general list
            for (Injured injuredPerson : injuredPeople){
                if (injuredPerson.injuryType == 0){
                    lightlyInjuredList.add(injuredPerson);
                }
            }
            
            

           
            
//            ACLMessage cfp = new ACLMessage(ACLMessage.CFP);
//    
//            cfp.setPerformative(ACLMessage.CFP);    
//            cfp.setProtocol(FIPANames.InteractionProtocol.FIPA_CONTRACT_NET);
//                            // We want to receive a reply in 10 secs
//            cfp.setReplyByDate(new Date(System.currentTimeMillis() + 10000));
//            cfp.setReplyWith(MessageContent.FCN);
//                           
//            try {
//                msg.setContentObject((Serializable) injuredPeople);
//            } catch (IOException ex) {
//                Logger.getLogger(ReceiveInjuredListBehaviour.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            
            RACoord.addBehaviour(new RunContractNetInitiator());
            
            
            
            }catch (UnreadableException e){
                e.printStackTrace();}
       
    }
}
