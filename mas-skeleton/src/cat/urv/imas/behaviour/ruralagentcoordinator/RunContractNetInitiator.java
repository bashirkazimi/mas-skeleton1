/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.behaviour.ruralagentcoordinator;
import jade.core.Agent;
import jade.core.AID;
import jade.lang.acl.ACLMessage;
import jade.proto.ContractNetInitiator;
import jade.domain.FIPANames;

import java.util.Date;
import java.util.Vector;
import java.util.Enumeration;
import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.RACoordinator;
import cat.urv.imas.agent.RuralAgent;
import cat.urv.imas.agent.UtilsAgents;
import cat.urv.imas.onthology.Injured;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.utils.Pair;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPANames;
import jade.proto.ContractNetInitiator;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;
import jade.lang.acl.MessageTemplate;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Bashir
 */
public class RunContractNetInitiator extends OneShotBehaviour {
    
    // http://www.upv.es/sma/plataformas/jade/programmersguide.pdf 
    // PAG 38
//    ACLMessage cfp;
//    Map<AID, List<Pair>> bids;
//    public RunContractNetInitiator(ACLMessage cfp) {
//        super();
//        this.cfp = cfp;
//        bids = new HashMap<>();
//    }
    
    
     @Override
    public void action() {

        RACoordinator RACoord = (RACoordinator)myAgent;
        LinkedList<AID> ruralAgents = RACoord.getRAList();
         
        for (AID ruralAID: ruralAgents){
            ACLMessage contract = new ACLMessage(ACLMessage.REQUEST);        
            contract.addReceiver(ruralAID);
            contract.setContent(MessageContent.FCN);
            /*try {
                auction.setContentObject(hcoord.getGame());
            } catch (IOException ex) {
                ex.printStackTrace();
            }*/
            RACoord.send(contract);
            RACoord.log("Sending contract to " + ruralAID.getLocalName());
        }
        
        HashMap<AID,LinkedList<Pair>> ruralBids = new HashMap<>();
//        HashMap<Injured, Pair> injuredBidsMap = new HashMap<>();

        for (AID ruralAID : RACoord.getRAList()){
            // Message bid template
            MessageTemplate msbt = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.AGREE),
                                                        MessageTemplate.MatchSender(ruralAID));
            
            ACLMessage msgBid = myAgent.blockingReceive(msbt);
            RACoord.log("Received contract bid from: "+msgBid.getSender().toString());
        try {
           
                LinkedList<Pair> content = (LinkedList<Pair>) msgBid.getContentObject();
                //Map<Injured,Map<AID,Integer>> bidContent = (Map<Injured,Map<AID,Integer>>) content;
                ruralBids.put(ruralAID, content);

            } catch (Exception e) {
            RACoord.errorLog(e.getMessage());
            e.printStackTrace();
            }
 
        }
        
        
        
            // SEND PROPOSAL TO COORDINATOR
            RACoord.log("content of Hashmap "+ruralBids.toString());
            ACLMessage contractnetResult = new ACLMessage(MessageContent.RURAL_PP);
            contractnetResult.setContent(MessageContent.FCN_READY);
            ServiceDescription searchCriterion = new ServiceDescription();
            searchCriterion.setType(AgentType.COORDINATOR.toString());
            AID coordinatorAgent = UtilsAgents.searchAgent(RACoord, searchCriterion);
            contractnetResult.addReceiver(coordinatorAgent);
            try{
            contractnetResult.setContentObject((Serializable)ruralBids);
            } catch (Exception e){
                RACoord.errorLog(e.getMessage());
                e.printStackTrace();
            }
            RACoord.send(contractnetResult);
        
            
            
            // Now we wait for the response of the winner among rural agents from coordinator;
            ServiceDescription searchCriterionCA = new ServiceDescription();
            searchCriterionCA.setType(AgentType.COORDINATOR.toString());
            AID CAAID = UtilsAgents.searchAgent(RACoord, searchCriterion);

            MessageTemplate mstAward = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.CONFIRM),
                                                        MessageTemplate.MatchSender(CAAID));
            
            ACLMessage msgBid = myAgent.blockingReceive(mstAward);
            
            // SEND THE WINNERS BACK;
            RACoord.log("Got awarded rural agents from CA");
            Map<AID,Integer> content = null;
            try{
                content = (Map<AID,Integer>)msgBid.getContentObject();
            }catch(Exception e){
                RACoord.log("Error receiving the awarded helicopters");
                e.printStackTrace();
            }
            
            // Keeping track of awarded rurals
            List<AID> awardedRurals = new LinkedList<>();
            
            Iterator it =content.entrySet().iterator();
            while(it.hasNext()){
                Map.Entry<AID, Integer> nextSet = (Map.Entry<AID, Integer>) it.next();
                ACLMessage awardWinner = new ACLMessage(ACLMessage.CONFIRM); 
                RACoord.log("Receiver of award message: "+nextSet.getKey());
                awardWinner.addReceiver(nextSet.getKey());
                try {
                    awardWinner.setContentObject(nextSet.getValue());
                } catch (Exception ex) {
                    ex.printStackTrace();
                    RACoord.log("Error when setting the award winners of HA");
                }
                RACoord.send(awardWinner);
                awardedRurals.add(nextSet.getKey());
                
            }
            
            // Send info back to those not awarded
            for (AID rural : RACoord.getRAList()){
                if (!awardedRurals.contains(rural)){
                    ACLMessage notifyNonWinner = new ACLMessage(ACLMessage.CONFIRM); 
                    notifyNonWinner.addReceiver(rural);
                    Integer ignoreMessage = -1;
                    try {
                        notifyNonWinner.setContentObject(ignoreMessage);
                    } catch (IOException ex) {
                         RACoord.log("Error sending unawarded rural message");
                         ex.printStackTrace();
                    }
                    
                    RACoord.send(notifyNonWinner);
                }
            }
            
            myAgent.addBehaviour(new WaitForInfoFromRuralAgentsBehaviour());
            
    }

}