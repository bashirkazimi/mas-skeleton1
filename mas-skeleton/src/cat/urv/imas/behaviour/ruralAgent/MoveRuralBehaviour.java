/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.behaviour.ruralagent;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.RuralAgent;
import cat.urv.imas.map.Cell;
import cat.urv.imas.map.CellType;
import cat.urv.imas.map.MountainHutCell;
import cat.urv.imas.map.PathCell;
import cat.urv.imas.onthology.InfoAgent;
import cat.urv.imas.onthology.Injured;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Bashir
 */
public class MoveRuralBehaviour extends OneShotBehaviour{

    @Override
    public void action() {
        RuralAgent rural = (RuralAgent) myAgent;
        boolean delete = false;
        
        //Check what is doing
        if (!rural.isComingBack()){
            // Check if injured is still alive
            if (isTargetAlive(rural)){
                // Move the rural agent
                Move(rural);
                // Check if rural is in target cell
                if (isTargetCell(rural)){
                    // Pick up!
                    PickUp(rural);
                    // Set the new path to save the injured
                    setPathToHut(rural);
                }
            } else{
                // Come back to mountain hut
                setPathToHut(rural);
            }
        } else{
            Move(rural);
            // Check if it has arrive to the mountain hut
            if (isTargetCell(rural)){
                delete = true;
            }
        }
        sendInformation(rural,delete);
        rural.addBehaviour(new RuralAgentContractNetResponderBehaviour());
    }
    
    
    public boolean isTargetAlive (RuralAgent rural){
        boolean targetAlive = false;
        if (rural.getTargetCell()!=null) {
            List<Injured> listInjured = rural.getGame().getInjuredPeopleList();
            if(listInjured.contains(rural.getTargetInjured())){
                targetAlive = true;
            }
        }
        return targetAlive;
    }
    
    public boolean isTargetCell (RuralAgent rural){
        // Check if rural is over the target
        boolean targetCell = false;
        if (rural.getCurrentCell().getCol() == rural.getTargetCell().getCol()){
            if (rural.getCurrentCell().getRow() == rural.getTargetCell().getRow()){
                targetCell = true;
            }
        }
        return targetCell;
    }
    
    
    public void Move(RuralAgent rural){
        Cell nextPos = rural.getFirstCellInPath();
        if (nextPos!=null){
            int nextrow = nextPos.getRow();
            int nextcol = nextPos.getCol();
            int actualrow = rural.getCurrentCell().getRow();
            int actualcol = rural.getCurrentCell().getCol();
            InfoAgent hc = new InfoAgent(AgentType.RURAL_AGENT,rural.getAgentAID());
            
            //Move the agent in the game
            try {
                rural.getGame().getMap()[actualrow][actualcol].removeAgent(hc);
                rural.getGame().getMap()[nextrow][nextcol].addAgent(hc);
                rural.setCurrentCell(nextPos);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void PickUp(RuralAgent rural){
        // Pick up an injured
        int actualrow = rural.getCurrentCell().getRow();
        int actualcol = rural.getCurrentCell().getCol();
        PathCell state = (PathCell)rural.getGame().getMap()[actualrow][actualcol];
        List<Injured> injuredList = state.getOurInjuredPeople();
        Injured guy;
        for(int i=0;i<injuredList.size();i++){
            guy = injuredList.get(i);
            if (guy.equals(rural.getTargetInjured())){
                // Edit parameters
                rural.setPickedUp(guy);
                guy.pickedUp = true;
                PathCell tmp = (PathCell)rural.getGame().getMap()[actualrow][actualcol];
                tmp.removeInjured(guy);
            }
        }        
    }
        
    
    public void setPathToHut(RuralAgent rural){
        //Find the nearest hospital
        Cell target = null;
        Cell pos = rural.getCurrentCell();
        target = rural.getMyhut();
        
        rural.getPath().clear();
        List<Cell> path = rural.getshortestpath(rural.getCurrentCell(),rural.getMyhut());
        rural.setPath(path);
        rural.setComingBack(true);
        rural.setTargetCell(rural.getMyhut());
    }
    
    
    public void sendInformation(RuralAgent rural, boolean empty){
        List<Injured> pickedUp = new LinkedList<>();
        pickedUp.add(rural.getPickedUp());
        if (empty){
            for (Injured picked:pickedUp){
                picked.reachedHospital = true;
            }
        }

        ACLMessage info = new ACLMessage(ACLMessage.INFORM);        
        info.addReceiver(rural.getRACoordinator());
        try {
            info.setContentObject((Serializable) pickedUp);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        rural.send(info);
        rural.log("SENT INJURED INFO FROM: "+rural.getLocalName());
        if (empty){
            rural.setPickedUp(null);
            rural.setComingBack(false);
            rural.setTargetCell(null);
            rural.setTargetInjured(null);
        }
    }
}
