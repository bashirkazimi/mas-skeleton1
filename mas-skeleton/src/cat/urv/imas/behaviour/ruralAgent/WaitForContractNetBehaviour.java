/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.behaviour.ruralagent;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.RuralAgent;
import cat.urv.imas.map.Cell;
import cat.urv.imas.onthology.Injured;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.utils.Pair;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPANames;
import jade.domain.FIPANames.InteractionProtocol;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;


import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import jade.proto.ContractNetResponder;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andres
 */
public class WaitForContractNetBehaviour extends OneShotBehaviour{
    
    @Override
    public void action(){
        RuralAgent ruralagent = (RuralAgent) myAgent;
        
        ruralagent.log("COMING HERE?");
//        MessageTemplate template = MessageTemplate.and(
//				MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_CONTRACT_NET),
//				MessageTemplate.MatchPerformative(ACLMessage.CFP) );
//        
//        MessageTemplate template = MessageTemplate.and(
//                   MessageTemplate.MatchPerformative(ACLMessage.CFP),
//                        MessageTemplate.MatchProtocol(InteractionProtocol.FIPA_CONTRACT_NET)
////                        MessageTemplate.MatchOntology(area.getId())    
//                   );
        ruralagent.addBehaviour(new RuralAgentContractNetResponderBehaviour());
        
//        ruralagent.addBehaviour(new ContractNetResponder(ruralagent, template) {
//                        public int checkDistance(int row, int col, RuralAgent ruralagent) {        
//                            // Helicopter is actually full
//                            if (ruralagent.isBusy()){
//                                return -1;
//                            }
////                            Queue <Cell> path = new LinkedList<>();
////                            Cell pos = ruralagent.getCurrentCell();
//                            // Rerturn Manhattan distance
//                            return ruralagent.distanceBetweenCells(row,col); 
//                        }
//			@Override
//			protected ACLMessage handleCfp(ACLMessage cfp) throws NotUnderstoodException, RefuseException {
//                            RuralAgent ruralA = (RuralAgent)myAgent;
//                            ruralA.log("CFP received injured list from "+cfp.getSender().getName()/*+". Action is "+cfp.getContent()*/);
//                            List<Injured> injuredList = new ArrayList<>();
//                            try {
//                                Object content = cfp.getContentObject();
//                                injuredList = (List<Injured>) content;
//                            } catch (UnreadableException ex) {
//                                Logger.getLogger(WaitForContractNetBehaviour.class.getName()).log(Level.SEVERE, null, ex);
//                            }
//					// We provide a proposal
//                            
//                            LinkedList<Pair> proposal = new LinkedList<>();
//                            ruralA.log("contractnet received ");
//                            // Compute proposals to each injured
//                            for (Injured injuredPerson : ruralA.getGame().getInjuredPeopleList()){
//                                // Here we will compute the bid
//                                proposal.add(new Pair(injuredPerson, checkDistance(injuredPerson.row,injuredPerson.col, ruralA)));
//                            }            
//                                        
//                            ruralA.log("Proposing propose");
//                            ACLMessage propose = cfp.createReply();
//                            propose.setPerformative(ACLMessage.PROPOSE);
//                            propose.setContent(String.valueOf(proposal));
//                            return propose;
//				
//				
////					// We refuse to provide a proposal
////					System.out.println("Agent "+getLocalName()+": Refuse");
////					throw new RefuseException("evaluation-failed");
//				
//			}
//
//			@Override
//			protected ACLMessage handleAcceptProposal(ACLMessage cfp, ACLMessage propose,ACLMessage accept) throws FailureException {
//				System.out.println("Agent "+getLocalName()+": Proposal accepted");
//				if (performAction()) {
//					System.out.println("Agent "+getLocalName()+": Action successfully performed");
//					ACLMessage inform = accept.createReply();
//					inform.setPerformative(ACLMessage.INFORM);
//					return inform;
//				}
//				else {
//					System.out.println("Agent "+getLocalName()+": Action execution failed");
//					throw new FailureException("unexpected-error");
//				}	
//			}
//
//			protected void handleRejectProposal(ACLMessage cfp, ACLMessage propose, ACLMessage reject) {
//				System.out.println("Agent "+getLocalName()+": Proposal rejected");
//			}
//		} );
//        
//        ServiceDescription searchCriterionHAC = new ServiceDescription();
//        
//        searchCriterionHAC.setType(AgentType.RURAL_AGENT_COORDINATOR.toString());
//        MessageTemplate mtrs = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.REQUEST), 
//                                                    MessageTemplate.MatchSender(ruralagent.getRACoordinator()));
//        
//         ACLMessage msg = myAgent.blockingReceive(mtrs);
//         try{
//             Object content = (Object) msg.getContent();
//             if (content==null){
//                ruralagent.log("Null message");
//            }
//            if (content.equals(MessageContent.FPSB)){
//              LinkedList<Integer> contract = new LinkedList<>();
//              ruralagent.log("ContractNet recieved to "+ ruralagent.getLocalName());
//              for (Injured injuredPerson : ruralagent.getGame().getInjuredPeopleList()){
//                  
//                    // Here we would compute the ContractNet
//                    contract.add(10);
//                }
//                ACLMessage reply = msg.createReply();
//                reply.setPerformative(ACLMessage.AGREE);
//                reply.setContentObject(contract);
//                ruralagent.log("Reply ready");
//                ruralagent.send(reply);
//            }
//             
//                
//             
//         } catch (Exception e) {
//            ruralagent.errorLog(e.getMessage());
//            e.printStackTrace();
//         }
    }
}

