/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.behaviour.ruralagent;

import cat.urv.imas.agent.RuralAgent;
import cat.urv.imas.map.PathCell;
import cat.urv.imas.onthology.Injured;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bashir
 */
public class DecideBehaviour  extends OneShotBehaviour{

    @Override
    public void action() {
        RuralAgent ruralAgent = (RuralAgent) myAgent;
        ruralAgent.log("Award waiting for: "+ruralAgent.getLocalName());

        // Receive result of auction
        MessageTemplate mtrs = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.CONFIRM), 
                                                    MessageTemplate.MatchSender(ruralAgent.getRACoordinator()));

        
        // Receive the message from HAC regarding the auction
        ACLMessage msg = myAgent.blockingReceive(mtrs);
        ruralAgent.log("Award received for: "+ruralAgent.getLocalName());
        
        Integer id = null;
        Injured guy = null;
        
        try {
            id = (Integer) msg.getContentObject();
        } catch (UnreadableException ex) {
            ex.printStackTrace();
        }
        
        if (id!=-1){
            List<Injured> injureds = ruralAgent.getGame().getInjuredPeopleList();
            for (Injured i:injureds){
                if (i.id == id){
                    guy = i;
                    guy.assigned = true;
                }
            }
            
            // First clear path
            ruralAgent.getPath().clear();
            
            //Find new path to injured
            ruralAgent.computePath(guy.row, guy.col);
            //Set target cell
            ruralAgent.setTargetCell(new PathCell(guy.row,guy.col));
            // Set injured Target
            ruralAgent.setTargetInjured(guy);
            ruralAgent.setComingBack(false);            
        }
        ruralAgent.addBehaviour(new MoveRuralBehaviour());
    }
    
}
