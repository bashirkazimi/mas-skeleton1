/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.behaviour.ruralagent;

import cat.urv.imas.agent.RuralAgent;
import cat.urv.imas.onthology.Injured;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.utils.Pair;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPANames.InteractionProtocol;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bashir
 */
public class RuralAgentContractNetResponderBehaviour extends OneShotBehaviour{

    
//    ACLMessage cfp;
//    MessageTemplate template;
//    
//    public RuralAgentContractNetResponderBehaviour() {
//        this.template = tmplt;
//    }
    @Override
    public void action() {
        RuralAgent ruralAgent = (RuralAgent)myAgent;
        
        MessageTemplate template = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.REQUEST), 
                                                   MessageTemplate.and(MessageTemplate.MatchSender(ruralAgent.getRACoordinator()), MessageTemplate.MatchContent(MessageContent.FCN) ) );
        // GET THE CONTRACT FROM RACOORDINATOR
        ACLMessage cfp = ruralAgent.blockingReceive(template);
        ruralAgent.log("Got contract "+cfp.getContent());
        List<Injured> injuredPeople = new ArrayList<>();

        try {
            Object content = (Object) cfp.getContent();
            if (content==null){
                ruralAgent.log("Null message");
            }
            if (content.equals(MessageContent.FCN)) {
                LinkedList<Pair> bids = new LinkedList<>();
                ruralAgent.log("FPSB auction recieved to "+ruralAgent.getLocalName());
                // Compute bid to each injured
                for (Injured injuredPerson : ruralAgent.getGame().getInjuredPeopleList()){
                    // Here we will compute the bid
                    bids.add(new Pair(injuredPerson, checkDistance(injuredPerson.row,injuredPerson.col, ruralAgent)));
                }
                // Send bids to Helicopter Coordinator
                ACLMessage reply = cfp.createReply();
                reply.setPerformative(ACLMessage.AGREE);
                reply.setContentObject((Serializable)bids);
                ruralAgent.log("Reply ready");
                ruralAgent.send(reply);
                ruralAgent.log("Bid sent. Waiting award"+ruralAgent.getLocalName());
                ruralAgent.addBehaviour(new DecideBehaviour());
            }
        } catch (Exception e) {
            ruralAgent.errorLog(e.getMessage());
            e.printStackTrace();
        } 
        
    }
    
    public int checkDistance(int row, int col, RuralAgent ruralagent) {        
        if (ruralagent.getTargetInjured() != null){
            return Integer.MAX_VALUE;
        }
        return ruralagent.distanceBetweenCells(row,col); 
    }
}