/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.behaviour.central;

import cat.urv.imas.agent.CentralAgent;
import jade.core.behaviours.OneShotBehaviour;
import cat.urv.imas.onthology.Injured;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.UtilsAgents;
import cat.urv.imas.map.Cell;
import cat.urv.imas.map.CellType;
import cat.urv.imas.map.PathCell;
import cat.urv.imas.onthology.MessageContent;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bashir
 */
public class FillTheMapBehaviour extends OneShotBehaviour {
    
    CentralAgent centralAgent;
    @Override
    public void action() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {
            Logger.getLogger(FillTheMapBehaviour.class.getName()).log(Level.SEVERE, null, ex);
        }
        centralAgent = (CentralAgent) getAgent();
        // Increment step counter
        centralAgent.getGame().incrementStep();
    
        centralAgent.updateGUI();
        if(centralAgent.getGame().finished()){
            centralAgent.log("End of Simulation!!");
            return;
        }
        centralAgent.log("Current Step: "+centralAgent.getGame().getCurrentStep());
        
          
        Random random = new Random();
        
        createAvalanches();
        
        // CREATE RANDOM INJURED PEOPLE;
        int max_injured = centralAgent.getGame().getTotalNumAgents();
        int num_injured_to_appear = random.nextInt(max_injured);
       // if(centralAgent.getGame().getCurrentStep()==1)
         //           centralAgent.getGame().createRandomInjured(2);

        centralAgent.getGame().createRandomInjured(num_injured_to_appear);
        
        
        ServiceDescription searchCriterionCoordinator = new ServiceDescription();
        searchCriterionCoordinator.setType(AgentType.COORDINATOR.toString());
        AID coordinator = UtilsAgents.searchAgent(myAgent, searchCriterionCoordinator);
        
        ACLMessage order = new ACLMessage(ACLMessage.REQUEST);
        order.addReceiver(coordinator);
        try{
            order.setContentObject(centralAgent.getGame());
        } catch (IOException e) {centralAgent.log(e.toString()); assert false;}
        centralAgent.log("SendingMap to " + coordinator.toString());
        myAgent.send(order);
        centralAgent.addBehaviour(new CentralBehaviour());

    }
    
    
    public void createAvalanches(){
        List<PathCell> pathList = new ArrayList<>(); 
        for(int i=0; i<centralAgent.getGame().getMap().length; i++){
            for(int j=0; j<centralAgent.getGame().getMap()[i].length; j++){
                if(centralAgent.getGame().getMap()[i][j].getCellType() == CellType.PATH){
                    pathList.add((PathCell) centralAgent.getGame().getMap()[i][j]);
                }
            }
        }
        Random rand = new Random();
        int numAvalancheThisStep = rand.nextInt(centralAgent.getGame().getMaxAvalanche());
        for(int i=0; i<numAvalancheThisStep;i++){
            int whichCell = rand.nextInt(pathList.size());
            if(pathList.get(whichCell).hasInjuredPeople()){
                centralAgent.getStatistics().addDiedPeople(pathList.get(whichCell).getOurInjuredPeople().size());
                pathList.get(whichCell).removeDead(pathList.get(whichCell).getOurInjuredPeople());
            }
            pathList.get(whichCell).setAvalanche(true);
            pathList.get(whichCell).setAvalancheSpan(centralAgent.getGame().getCurrentStep()+centralAgent.getGame().getAvalancheDuration());
        }
    }
}
