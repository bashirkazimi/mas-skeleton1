/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.behaviour.central;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.CentralAgent;
import cat.urv.imas.gui.Statistics;
import cat.urv.imas.onthology.Injured;
import cat.urv.imas.onthology.MessageContent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Bashir
 */
public class CentralBehaviour extends OneShotBehaviour {

    @Override
    public void action() {
        CentralAgent centralAgent = (CentralAgent) myAgent;
        centralAgent.log("Waiting in CentralBehaviour");

        //MessageTemplate receivedMsg = MessageTemplate.and(MessageTemplate.MatchPerformative(MessageContent.READY),
        //                                          MessageTemplate.MatchReplyWith(MessageContent.SAVED));
        MessageTemplate receivedMsg = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
                                                  MessageTemplate.MatchSender(centralAgent.getCoordinatorAgent()));        
        ACLMessage object = myAgent.blockingReceive(receivedMsg);
        centralAgent.log("Received list of injured from coordinator: ");
//        MessageTemplate mtrp = MessageTemplate.and(MessageTemplate.MatchPerformative(MessageContent.READY),
//                                                  MessageTemplate.MatchReplyWith(MessageContent.PICK_UP));
//        ACLMessage pickedUpReadyMsg = myAgent.blockingReceive(mtrp);
        

        try {
            Object content = object.getContentObject();
            if (content != null) {
                Map<AgentType, LinkedList<Injured>> injuredList = (Map<AgentType, LinkedList<Injured>>) content;
                Statistics statistics = centralAgent.getStatistics();
                List<Injured> helicoptersList = injuredList.get(AgentType.HELICOPTER);
                List<Injured> ruralsList = injuredList.get(AgentType.RURAL_AGENT);
                int rescuedByHelicopter = 0;
                int rescuedByRural = 0;
                int stepsToFreeze = centralAgent.getGame().getStepsToFreeze();
//                List<Injured> toRemove = new ArrayList<>();
                for(int i=0; i<helicoptersList.size(); i++){
                    if(helicoptersList.get(i).reachedHospital){
                        rescuedByHelicopter++;
                        int steps = stepsToFreeze - (helicoptersList.get(i).timeOfDeath - centralAgent.getGame().getCurrentStep());
//                        centralAgent.getGame().removeRescuedPeople(helicoptersList.get(i));
                        statistics.addFirstVisit(steps);
                    }
                     if(helicoptersList.get(i).pickedUp || helicoptersList.get(i).assigned)
                        centralAgent.getGame().removeInjuredFromListCozItHasBeenTakenCareOf(helicoptersList.get(i));
//                   
//                    else{
//                        toRemove.add(helicoptersList.get(i));
//                    }
                }
                statistics.addHospitalRescue(rescuedByHelicopter);
                
                
                for(int i=0; i<ruralsList.size(); i++){
                    if(ruralsList.get(i).reachedHospital){
                        rescuedByRural++;
                        int steps = stepsToFreeze - (ruralsList.get(i).timeOfDeath - centralAgent.getGame().getCurrentStep());
//                        centralAgent.getGame().removeRescuedPeople(ruralsList.get(i));
                        statistics.addFirstVisit(steps);
                    }
                    if(ruralsList.get(i).pickedUp || ruralsList.get(i).assigned)
                        centralAgent.getGame().removeInjuredFromListCozItHasBeenTakenCareOf(ruralsList.get(i));
//                    else{
//                        toRemove.add(helicoptersList.get(i));
//                    }
                }
                statistics.addRuralAgentRescue(rescuedByRural);
                
//                for(int i=0; i<toRemove.size();i++){
////                    centralAgent.getGame().removePickedUpInjuredPeople(toRemove.get(i));
//                }
                
                // CLEAN DEAD PEOPLE IF NOT SAVED;
                int num_dead = centralAgent.getGame().cleanDeadPeopleAndAvalanches();
                centralAgent.getStatistics().addDiedPeople(num_dead);
                
            }
            


        } catch (UnreadableException e) {
            e.printStackTrace();
        }
        
        centralAgent.addBehaviour(new FillTheMapBehaviour());

    }
    
    
}
