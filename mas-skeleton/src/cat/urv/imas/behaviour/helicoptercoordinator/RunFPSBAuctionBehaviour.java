/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.behaviour.helicoptercoordinator;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.HCoordinator;
import cat.urv.imas.agent.Helicopter;
import cat.urv.imas.agent.UtilsAgents;
import cat.urv.imas.onthology.Injured;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.utils.Pair;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author juarugui
 */
public class RunFPSBAuctionBehaviour extends OneShotBehaviour {

    @Override
    public void action() {
        HCoordinator hcoord = (HCoordinator) myAgent;
        
        //Send information to all the helicopter agents
        LinkedList<AID> helicopters = hcoord.getHAList();
        for (AID helAID: helicopters){
            ACLMessage auction = new ACLMessage(ACLMessage.REQUEST);        
            auction.addReceiver(helAID);
            auction.setContent(MessageContent.FPSB);
            hcoord.send(auction);
        }
        // Injured people
        List<Injured> injuredPeople = hcoord.getGame().getInjuredPeopleList();
        HashMap<AID,LinkedList<Pair>> helicopterTotalBids = new HashMap<>();
        HashMap<Injured, Pair> injuredBidsMap = new HashMap<>();

        for (AID helicopterAID : hcoord.getHAList()){
            // Message bid template
            MessageTemplate msbt = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.AGREE),
                                                        MessageTemplate.MatchSender(helicopterAID));
            
            ACLMessage msgBid = myAgent.blockingReceive(msbt);
            hcoord.log("Received bid from: "+msgBid.getSender().toString());
        try {
           
                LinkedList<Pair> content = (LinkedList<Pair>) msgBid.getContentObject();
                //Map<Injured,Map<AID,Integer>> bidContent = (Map<Injured,Map<AID,Integer>>) content;
                helicopterTotalBids.put(helicopterAID, content);

            } catch (Exception e) {
            hcoord.errorLog(e.getMessage());
            e.printStackTrace();
            }  
        }
        
        
            ACLMessage auctionResult = new ACLMessage(MessageContent.HELICOPTER_PP);
            //auctionResult.setContent(MessageContent.FPSB_READY);
            ServiceDescription searchCriterion = new ServiceDescription();
            searchCriterion.setType(AgentType.COORDINATOR.toString());
            AID coordinatorAgent = UtilsAgents.searchAgent(hcoord, searchCriterion);
            auctionResult.addReceiver(coordinatorAgent);
            try{
                auctionResult.setContentObject((Serializable)helicopterTotalBids);
            } catch (Exception e){
                hcoord.errorLog(e.getMessage());
                e.printStackTrace();
            }
            hcoord.send(auctionResult);
            hcoord.log("Auction result sent to CA"+helicopterTotalBids.toString());
            
            // Now we wait for the response of the winner from helicopters
            ServiceDescription searchCriterionCA = new ServiceDescription();
            searchCriterionCA.setType(AgentType.COORDINATOR.toString());
            AID CAAID = UtilsAgents.searchAgent(hcoord, searchCriterion);

            MessageTemplate mstAward = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.CONFIRM),
                                                        MessageTemplate.MatchSender(CAAID));
            
            ACLMessage msgAward = myAgent.blockingReceive(mstAward);
            Map<AID,Integer> content = null;
            try{
                content = (Map<AID,Integer>)msgAward.getContentObject();
            }catch(Exception e){
                hcoord.log("Error receiving the awarded helicopters");
                e.printStackTrace();
            }
            
            // For each helicopter AID we send a message containing the ID
            // of the injured person to go to
            List<AID> awardedHelicopters = new LinkedList<>();
            Iterator it =content.entrySet().iterator();
            while(it.hasNext()){
                Map.Entry<AID, Integer> nextSet = (Map.Entry<AID, Integer>) it.next();
                ACLMessage awardWinner = new ACLMessage(ACLMessage.CONFIRM); 

                awardWinner.addReceiver(nextSet.getKey());
                try {
                    awardWinner.setContentObject(nextSet.getValue());
                } catch (Exception ex) {
                    ex.printStackTrace();
                    hcoord.log("Error when setting the award winners of HA");
                }
                hcoord.log("AWARD WINNER: "+ nextSet.getValue());
                hcoord.send(awardWinner);
                // keep track of awarded helicopters. We need messages to all of them.
                awardedHelicopters.add(nextSet.getKey());
            }
            
            // Send no task to those not awarded
            for (AID helicopter : hcoord.getHAList()){
                if (!awardedHelicopters.contains(helicopter)){
                    ACLMessage notifyNonWinner = new ACLMessage(ACLMessage.CONFIRM); 
                    notifyNonWinner.addReceiver(helicopter);
                    Integer ignoreMessage = -1;
                    try {
                        notifyNonWinner.setContentObject(ignoreMessage);
                    } catch (IOException ex) {
                         hcoord.log("Error sending unawarded helicopters message");
                         ex.printStackTrace();
                    }
                    
                    hcoord.send(notifyNonWinner);
                }
            }
    
    // Wait for results from helicopters
    myAgent.addBehaviour(new WaitForInfoFromHelicoptersBehaviour());
   
    }
    

}
