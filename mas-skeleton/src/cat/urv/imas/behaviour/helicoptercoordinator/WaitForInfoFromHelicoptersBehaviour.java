/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.behaviour.helicoptercoordinator;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.HCoordinator;
import cat.urv.imas.agent.UtilsAgents;
import cat.urv.imas.onthology.Injured;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import java.io.IOException;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author juarugui
 */
public class WaitForInfoFromHelicoptersBehaviour extends OneShotBehaviour {

    @Override
    public void action() {
        
        HCoordinator hcoord = (HCoordinator) myAgent;
        LinkedList<Injured> result = new LinkedList();
        for (AID helicopterAID : hcoord.getHAList()){
            // Message bid template
            MessageTemplate msbt = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
                                                        MessageTemplate.MatchSender(helicopterAID));
            
            ACLMessage msg = myAgent.blockingReceive(msbt);
            hcoord.log("Received from "+helicopterAID.getLocalName());
            try {
                Object obj = (Object) msg.getContentObject();
                result.addAll((LinkedList<Injured>) obj);
            } catch (UnreadableException ex) {
                Logger.getLogger(WaitForInfoFromHelicoptersBehaviour.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        hcoord.log("oso helicopter: "+result.toString());
        
        //  Search for Coordinator Agent
        ServiceDescription searchCriterionCoordinatorAgent = new ServiceDescription();
        searchCriterionCoordinatorAgent.setType(AgentType.COORDINATOR.toString());
        
        AID coordAgent = UtilsAgents.searchAgent(hcoord, searchCriterionCoordinatorAgent);
        
        ACLMessage data = new ACLMessage(ACLMessage.INFORM);
        data.addReceiver(coordAgent);
        try {
            data.setContentObject((Serializable) result);
            hcoord.log("Data sent to COORDINATOR after info from helicopter: "+result.toString());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        hcoord.send(data);
        
        hcoord.addBehaviour(new ReceiveInjuredListBehaviour());
    }
    
}
