/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.behaviour.helicoptercoordinator;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.HCoordinator;
import cat.urv.imas.agent.UtilsAgents;
import cat.urv.imas.onthology.GameSettings;
import cat.urv.imas.onthology.Injured;
import cat.urv.imas.onthology.MessageContent;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import static jade.lang.acl.ACLMessage.REQUEST;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author juarugui
 */
public class ReceiveInjuredListBehaviour extends OneShotBehaviour {

    @Override
    public void action() {
        HCoordinator HACoord = (HCoordinator) myAgent;
        
        //  Search for Coordinator Agent
        ServiceDescription searchCriterionCoordinatorAgent = new ServiceDescription();
        searchCriterionCoordinatorAgent.setType(AgentType.COORDINATOR.toString());
        
        AID coordAgent = UtilsAgents.searchAgent(HACoord, searchCriterionCoordinatorAgent);
        
        // Waiting for CoordinatorAgent to send list of injured people
        MessageTemplate mstListInjured = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.REQUEST),
                                                             MessageTemplate.MatchSender(coordAgent));
        
        // Blocked waiting for message from Coordinator Agent with the list of injured people
        ACLMessage msg = myAgent.blockingReceive(mstListInjured);
        ACLMessage reply = msg.createReply();
        
        // Once received, obtained content of the message
        try{
            Object content = msg.getContentObject();
   
            // add if content null?
            GameSettings game = (GameSettings) content;
            // Get the injured people list
            List<Injured> injuredPeople = game.getInjuredPeopleList();
            ArrayList<Injured> severelyInjuredPeople = new ArrayList<Injured>();
            
            // Select the severly injured people from the general list
            for (Injured injuredPerson : injuredPeople){
                if (injuredPerson.injuryType == 1){
                    severelyInjuredPeople.add(injuredPerson);
                }
            }
            
            // Affirmative reply when the severely injured list is built
            //reply.setPerformative(ACLMessage.AGREE);
            
            // If number of injured people in this iteration is different than
            // the number of injured people in last iteration, we proceed to
            // run a FPSB auction
            
            myAgent.addBehaviour(new RunFPSBAuctionBehaviour());
            // Start FPBS Behaviour
            
        } catch (UnreadableException e){
            e.printStackTrace();
        }
        
    }
    
}
