/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.behaviour.helicopter;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.Helicopter;
import cat.urv.imas.map.Cell;
import cat.urv.imas.map.HospitalCell;
import cat.urv.imas.map.InjuredPeople;
import cat.urv.imas.map.PathCell;
import cat.urv.imas.onthology.InfoAgent;
import cat.urv.imas.onthology.Injured;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miguel
 */
public class MoveBehaviour extends OneShotBehaviour {

    @Override
    public void action() {
        Helicopter helicopter = (Helicopter) myAgent;
        boolean delete = false;
        // Check what helicopter is doing
        if (!helicopter.isHospital()){
            // Check if injured is still alive
            if (isTargetAlive(helicopter)){
                // Move the helicopter agent
                Move(helicopter);
                //Check if helicopter is in target cell
                if (isTargetCell(helicopter)){
                   // Pick up!
                   PickUp(helicopter);
                   // Set the new path to save the injured
                   setPathToHospital(helicopter);
                }
            } else {
                //Come back to hospital
                setPathToHospital(helicopter);
            }
        } else {
            Move(helicopter);
            //Check if it has arrived to the hospital
            if (isTargetCell(helicopter)){
                // Send saved injureds
                delete = true;
            }
        }
        sendInformation(helicopter, delete);
        helicopter.addBehaviour(new WaitForAuctionBehaviour());
    }
    

    public boolean isTargetAlive (Helicopter helicopter) {
        // Check if person still there
        boolean targetAlive = false;
        if (helicopter.getTargetCell()!=null) {
            List<Injured> listInjured = helicopter.getGame().getInjuredPeopleList();
                if(listInjured.contains(helicopter.getTargetInjured())){
                    targetAlive = true;
                }
        }
        return targetAlive;
    }
    
    public boolean isTargetCell (Helicopter helicopter){
        // Check if helicopter is in the target cell
        boolean targetCell = false;
        if (helicopter.getCurrentCell().getCol() == helicopter.getTargetCell().getCol()){
            if (helicopter.getCurrentCell().getRow() == helicopter.getTargetCell().getRow()){
                targetCell = true;
            }
        }
        return targetCell;
    }
    
    public void Move(Helicopter helicopter){
        Cell nextPos = helicopter.getPath().poll();
        if (nextPos!=null){
            int nextrow = nextPos.getRow();
            int nextcol = nextPos.getCol();
            int actualrow = helicopter.getCurrentCell().getRow();
            int actualcol = helicopter.getCurrentCell().getCol();
            InfoAgent hc = new InfoAgent(AgentType.HELICOPTER,helicopter.getAgentAID());
            
            // Move the agent in the game
            try {
                helicopter.getGame().getMap()[actualrow][actualcol].removeAgent(hc);
                helicopter.getGame().getMap()[nextrow][nextcol].addAgent(hc);
                helicopter.setCurrentCell(nextPos);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }       
    }
    
    
    public void PickUp(Helicopter helicopter){
        // Pick up an injured
        int actualrow = helicopter.getCurrentCell().getRow();
        int actualcol = helicopter.getCurrentCell().getCol();
        PathCell state = (PathCell)helicopter.getGame().getMap()[actualrow][actualcol];
        List<Injured> injuredList = state.getOurInjuredPeople();
        Injured guy;
        //Look for the injured
        for(int i=0;i<injuredList.size();i++){
            guy = injuredList.get(i);
            if (guy.equals(helicopter.getTargetInjured())){
                // Edit parameters
                helicopter.getPickedUp().add(guy);
                guy.pickedUp = true;
                PathCell tmp = (PathCell)helicopter.getGame().getMap()[actualrow][actualcol];
                tmp.removeInjured(guy);
            }
        }
    }
    
    public void setPathToHospital(Helicopter helicopter){
        Cell target = null;
        Cell pos = helicopter.getCurrentCell();
        target = helicopter.getMyhospital();
        
        helicopter.getPath().clear();
        Queue<Cell> path = DecideActionBehaviour.computePath(target.getRow(), target.getCol(), helicopter);
        helicopter.setPath(path);
        helicopter.setHospital(true);
        helicopter.setTargetCell(helicopter.getMyhospital());
    }
    
    public void sendInformation(Helicopter helicopter, boolean empty){
        List<Injured> pickedUp = helicopter.getPickedUp();
        if (empty){
            for (Injured picked:pickedUp){
                picked.reachedHospital = true;
            }
        }

        ACLMessage info = new ACLMessage(ACLMessage.INFORM);        
        info.addReceiver(helicopter.getHCoordinator());
        try {
            info.setContentObject((Serializable) pickedUp);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        helicopter.send(info);
        if (empty){
            helicopter.getPickedUp().clear();
            helicopter.setHospital(false);
            helicopter.setTargetCell(null);
            helicopter.setTargetInjured(null);
        }
    }
        
}