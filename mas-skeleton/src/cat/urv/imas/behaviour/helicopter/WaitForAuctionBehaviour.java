/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.behaviour.helicopter;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.Helicopter;
import cat.urv.imas.map.Cell;
import cat.urv.imas.onthology.Injured;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.utils.Pair;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

/**
 *
 * @author juarugui
 */
public class WaitForAuctionBehaviour extends OneShotBehaviour {

    @Override
    public void action() {
        
        Helicopter helicopter = (Helicopter) myAgent;

        // We will wait for the FPSB at each iteration.
        MessageTemplate mtrs = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.REQUEST), 
                                                    MessageTemplate.MatchSender(helicopter.getHCoordinator()));
        
        // Receive the message from HAC regarding the auction
        ACLMessage msg = myAgent.blockingReceive(mtrs);
        
        try {
            Object content = (Object) msg.getContent();
            if (content==null){
                helicopter.log("Null message");
            }
            if (content.equals(MessageContent.FPSB)) {
                LinkedList<Pair> bids = new LinkedList<>();
                // Compute bid to each injured
                for (Injured injuredPerson : helicopter.getGame().getInjuredPeopleList()){
                    // Here we will compute the bid
                    bids.add(new Pair(injuredPerson, checkDistance(injuredPerson.row,injuredPerson.col, helicopter)));
                }
                // Send bids to Helicopter Coordinator
                ACLMessage reply = msg.createReply();
                reply.setPerformative(ACLMessage.AGREE);
                reply.setContentObject((Serializable)bids);
                helicopter.send(reply);
                helicopter.addBehaviour(new DecideActionBehaviour());

            }
        } catch (Exception e) {
            helicopter.errorLog(e.getMessage());
            e.printStackTrace();
        }
        
    }
    
    
    public static int checkDistance(int row, int col, Helicopter helicopter) {        
        // Helicopter is actually full
        if (helicopter.getTargetInjured() != null){
            return Integer.MAX_VALUE;
        }
        Cell pos = helicopter.getCurrentCell();
        // Rerturn Manhattan distance
        return Math.abs(pos.getCol() - col) + Math.abs(pos.getRow() - row); 
    }
    
}
