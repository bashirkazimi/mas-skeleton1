/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.behaviour.helicopter;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.Helicopter;
import cat.urv.imas.map.Cell;
import cat.urv.imas.map.PathCell;
import cat.urv.imas.onthology.Injured;
import cat.urv.imas.onthology.MessageContent;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

/**
 *
 * @author Miguel
 */
public class DecideActionBehaviour extends OneShotBehaviour{

    @Override
    public void action() {
        Helicopter helicopter = (Helicopter) myAgent;

        // Receive result of auction
        MessageTemplate mtrs = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.CONFIRM), 
                                                    MessageTemplate.MatchSender(helicopter.getHCoordinator()));

        
        // Receive the message from HAC regarding the auction
        ACLMessage msg = myAgent.blockingReceive(mtrs);
        // Helicopter has won the auction
        Integer id = null;
        Injured guy = null;
        
            try {
                id = (Integer) msg.getContentObject();
            } catch (UnreadableException ex) {
                ex.printStackTrace();
            }
            if (id!=-1){
                List<Injured> injureds = helicopter.getGame().getInjuredPeopleList();
                for (Injured i:injureds){
                    if (i.id==id){
                        guy = i;
                        guy.assigned = true;
                    }
                }

                // First clear path
                helicopter.getPath().clear();

                // Find new path to injured
                helicopter.setPath(computePath(guy.row, guy.col, helicopter));
                //Set target cell
                helicopter.setTargetCell(new PathCell(guy.row,guy.col));
                //Set injured Target
                helicopter.setTargetInjured(guy);
                helicopter.setHospital(false);
            }
        helicopter.addBehaviour(new MoveBehaviour());

    }
    
    
    public static Queue<Cell> computePath(int row, int col, Helicopter helicopter){
        Queue<Cell> path = new LinkedList<>();
        int myrow = helicopter.getCurrentCell().getRow();
        int mycol = helicopter.getCurrentCell().getCol();
        int d = Math.abs(mycol- col) + Math.abs(myrow - row);
        int dx = col-mycol;
        int dy = row-myrow;
        int directionx = 1;
        int directiony = 1;
        // Check position if different than actual
        if (d > 0){
            if (dx < 0) {
                directionx = -1;
            }
            for (int i=0;i<Math.abs(dx);i++){
                mycol +=  directionx;
                Cell c = new PathCell(myrow, mycol);
                path.add(c);
            }
            if (dy <0) {
                directiony = -1;
            }
            for (int i=0;i<Math.abs(dy);i++){
                myrow +=  directiony;
                Cell c = new PathCell(myrow, mycol);
                path.add(c);
            }
        }
        return path;
    }
    
    
}
