/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.behaviour.coordinator;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.CoordinatorAgent;
import cat.urv.imas.agent.Helicopter;
import cat.urv.imas.onthology.Injured;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.utils.Pair;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author juarugui
 */
public class BuildGlobalPlanBehaviour extends OneShotBehaviour {

    @Override
    public void action() {
        CoordinatorAgent coordAgent = (CoordinatorAgent) myAgent;

        // Find RAC and HC
        Map<AgentType, AID> coordinators = coordAgent.getCoordinators();
        AID helicopterCoord = coordinators.get(AgentType.HELICOPTER_COORDINATOR);
        AID ruralCoord = coordinators.get(AgentType.RURAL_AGENT_COORDINATOR);

        // Waiting for Helicopter coordinator to send Helicopter Partial Plan
        MessageTemplate mtrs = MessageTemplate.and(MessageTemplate.MatchPerformative(MessageContent.HELICOPTER_PP),
                                                  MessageTemplate.MatchSender(helicopterCoord));
        ACLMessage helicopterPartialPlanMessage = myAgent.blockingReceive(mtrs);
        HashMap<AID,LinkedList<Pair>> object = null;
        try{
            object = (HashMap<AID,LinkedList<Pair>>) helicopterPartialPlanMessage.getContentObject();

        }catch(Exception e){
            e.printStackTrace();
        }
        // Create structure
        
        Map<Integer, List<Pair>> injuredPartialPlan = createStructure(coordAgent, object);
        Set<AID> aidset = object.keySet();
        Iterator aidsetIter = aidset.iterator();
            
        while(aidsetIter.hasNext()){
            AID helicopterAID = (AID)aidsetIter.next();
            List<Pair> listofPair = object.get(helicopterAID);
            for(int i=0; i<listofPair.size();i++ ){
                List<Pair> pairlist = new ArrayList<>();
                pairlist.add(new Pair(helicopterAID, listofPair.get(i).getR()));
                Injured inj = (Injured)listofPair.get(i).getL();

                    injuredPartialPlan.get(inj.id).add(new Pair(helicopterAID, listofPair.get(i).getR()));
            }
         }

        
        // Obtain partial plan from RAC
        MessageTemplate mtRA = MessageTemplate.and(MessageTemplate.MatchPerformative(MessageContent.RURAL_PP),
                                                  MessageTemplate.MatchSender(ruralCoord));
        ACLMessage ruralAgentPartialPlanMessage = myAgent.blockingReceive(mtRA);
        HashMap<AID,LinkedList<Pair>> objectRA = null;
        try{
            objectRA = (HashMap<AID,LinkedList<Pair>>) ruralAgentPartialPlanMessage.getContentObject();
            coordAgent.log("Content of RPP: "+objectRA.toString());

        }catch(Exception e){
            e.printStackTrace();
        }
        
        // Build structure again
        Map<Integer, List<Pair>> injuredPartialPlanRural = createStructure(coordAgent, objectRA);

        Set<AID> aidRuralset = objectRA.keySet();
        Iterator aidRuralsetIter = aidRuralset.iterator();
            
        while(aidRuralsetIter.hasNext()){
            AID ruralAID = (AID)aidRuralsetIter.next();
            List<Pair> listofPair = objectRA.get(ruralAID);
            for(int i=0; i<listofPair.size();i++ ){
                List<Pair> pairlist = new ArrayList<>();
                pairlist.add(new Pair(ruralAID, listofPair.get(i).getR()));
                Injured inj = (Injured)listofPair.get(i).getL();

                    injuredPartialPlanRural.get(inj.id).add(new Pair(ruralAID, listofPair.get(i).getR()));
            }
         }
        
        coordAgent.log("Final HashmapRural "+injuredPartialPlanRural.toString());
        
        
        
        // Compute content to send to the helicopter coordinator
        List<Map<AID,Integer>> awardMap = assignTotalTasks(coordAgent, injuredPartialPlan, injuredPartialPlanRural);
        
        // Send back the awards to HAC
        Map<AID,Integer> awardHelicopterMap = awardMap.get(0);
        ACLMessage replyH = helicopterPartialPlanMessage.createReply();
        replyH.setPerformative(ACLMessage.CONFIRM);
        try{
            replyH.setContentObject((Serializable)awardHelicopterMap);
        }catch(Exception e){
            coordAgent.log("Error sending the awards to HAC: ");
            e.printStackTrace();
        }
        
        // Send back the reply containing the awarded helicopters
        coordAgent.send(replyH);
        
        
        // Send back the awards to HAC
        Map<AID,Integer> awardRuralMap = awardMap.get(1);
        ACLMessage replyRA = ruralAgentPartialPlanMessage.createReply();
        replyRA.setPerformative(ACLMessage.CONFIRM);
        try{
            replyRA.setContentObject((Serializable)awardRuralMap);
        }catch(Exception e){
            coordAgent.log("Error sending the awards to RAC: ");
            e.printStackTrace();
        }
        
        // Send back the reply containing the awarded helicopters
        coordAgent.send(replyRA);

        myAgent.addBehaviour(new ReceiveInformationBehaviour());
    }
    
        public List<Map<AID,Integer>> assignTotalTasks(CoordinatorAgent coordAgent ,Map<Integer, List<Pair>> injuredHelicopterPartialPlan,
                                                    Map<Integer, List<Pair>> injuredRuralPartialPlan){
        
        List<Map<AID,Integer>> listOfAwards = new LinkedList<>();
        // keys of the hashmaps (both hashmaps have the same keys)
        Set<Integer> ids = injuredHelicopterPartialPlan.keySet();
        
        Iterator idIt = ids.iterator();
        Integer id;
        //
        List<Pair> helicopterPairList = new LinkedList<>();
        List<Pair> ruralPairList = new LinkedList<>();
        //
        List<AID> assignedAIDs = new LinkedList<>();
        //
        Map<AID,Integer> helicopterAwards = new HashMap<>();
        Map<AID,Integer> ruralAwards = new HashMap<>();
        //
        List<Injured> injuredPeopleList = coordAgent.getGame().getInjuredPeopleList();
        boolean jump;
        while(idIt.hasNext()){
            jump = false;
            id = (Integer)idIt.next();
            for (int j=0;j<injuredPeopleList.size();j++){
                Injured tmp = injuredPeopleList.get(j);
                if ((id==tmp.id)&&(tmp.assigned)){
                    jump = true;
                }
            }
         
            if (!jump){
                helicopterPairList = injuredHelicopterPartialPlan.get(id);
                ruralPairList = injuredRuralPartialPlan.get(id);
                
                // Sort the list by bids
                Collections.sort(helicopterPairList,new Comparator<Pair>(){
                    public int compare(Pair a, Pair b){
                        return (Integer)a.getR()-(Integer)b.getR();
                    }
                });
                
                Collections.sort(ruralPairList,new Comparator<Pair>(){
                    public int compare(Pair a, Pair b){
                        return (Integer)a.getR()-(Integer)b.getR();
                    }
                });
                
                

                for (Pair actual:helicopterPairList){
                    // we check if severely injured
                        // Is there a free agent?
                    int severity = 0;
                    for (int j=0;j<injuredPeopleList.size();j++){
                        Injured tmp = injuredPeopleList.get(j);
                        if ((id==tmp.id)&&(tmp.injuryType==1)){
                            severity = 1;
                        }
                    }
                    
                    if (severity==1){
                        if ((Integer)actual.getR() == Integer.MAX_VALUE)
                            break;
                        // If AID is free, add to list
                        if (!assignedAIDs.contains(actual.getL())){
                            assignedAIDs.add((AID) actual.getL());
                            helicopterAwards.put((AID)actual.getL(), id);
                            break;
                        }
                    }else{
                        for (Pair ruralPair : ruralPairList){
                            if ((Integer)ruralPair.getR() == Integer.MAX_VALUE)
                                break;
                            // If AID is free, add to list
                            if (!assignedAIDs.contains(ruralPair.getL())){
                                assignedAIDs.add((AID) ruralPair.getL());
                                ruralAwards.put((AID)ruralPair.getL(), id);
                                break;
                            }    
                        }
                    }
                }                
            }

        }
        listOfAwards.add(helicopterAwards);
        coordAgent.log("Value of helicopterawards: "+helicopterAwards.toString());
        listOfAwards.add(ruralAwards);
        coordAgent.log("Value of ruralawards: "+ruralAwards.toString());

        return listOfAwards;

}

        
        
        
        
        
        
    public Map<AID,Integer> assignTasks(CoordinatorAgent coordAgent ,Map<Integer, List<Pair>> injuredPartialPlan){
        Set<Integer> ids = injuredPartialPlan.keySet();
        Iterator idIt = ids.iterator();
        Integer id;
        List<Pair> pairList = new LinkedList<>();
        List<AID> assignedAIDs = new LinkedList<>();
        Map<AID,Integer> awards = new HashMap<>();
        List<Injured> injuredPeopleList = coordAgent.getGame().getInjuredPeopleList();
        boolean jump;
        while(idIt.hasNext()){
            jump = false;
            id = (Integer)idIt.next();
            for (int j=0;j<injuredPeopleList.size();j++){
                Injured tmp = injuredPeopleList.get(j);
                if ((id==tmp.id)&&(tmp.assigned)){
                    jump = true;
                }
            }
            // If injured is not assigned to any adgent
            if (!jump){
                // Select list of Pairs(AID,bid) for each injured
                pairList = injuredPartialPlan.get(id);

                // Sort the list by bids
                Collections.sort(pairList,new Comparator<Pair>(){
                    public int compare(Pair a, Pair b){
                        return (Integer)a.getR()-(Integer)b.getR();
                    }
                });
                // Select in order the bids until find a free AID
                for (Pair actual:pairList){
                    // Is there a free agent?
                    if ((Integer)actual.getR() == Integer.MAX_VALUE)
                        break;
                    // If AID is free, add to list
                    if (!assignedAIDs.contains(actual.getL())){
                        assignedAIDs.add((AID) actual.getL());
                        awards.put((AID)actual.getL(), id);
                        break;
                    }
                }
            }
        }
        return awards;
    }

    public Map<Integer, List<Pair>> createStructure (CoordinatorAgent coordAgent,HashMap<AID,LinkedList<Pair>> object){
        // Convert the hashmap to a hashmap with injured as keys
        Map<Integer, List<Pair>> injuredPartialPlan = new HashMap<>();
        Set<AID> aidset = object.keySet();

        Iterator aidsetIter = aidset.iterator();
        if(aidsetIter.hasNext()){
            AID dummyAID = (AID)aidsetIter.next();
            List<Pair> listofPair = object.get(dummyAID);
                for(int i=0; i<listofPair.size();i++ ){
                    List<Pair> pairlist = new ArrayList<>();
                    Injured inj = (Injured)listofPair.get(i).getL();
                    injuredPartialPlan.put(inj.id,pairlist);//.
                }
            }
        return injuredPartialPlan;
    }

}
