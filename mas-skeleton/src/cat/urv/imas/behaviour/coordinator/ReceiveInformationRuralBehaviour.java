/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.behaviour.coordinator;


import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.CoordinatorAgent;
import cat.urv.imas.agent.UtilsAgents;
import cat.urv.imas.onthology.Injured;
import cat.urv.imas.onthology.MessageContent;
import static cat.urv.imas.onthology.MessageContent.READY;
import cat.urv.imas.utils.Pair;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andres
 */
public class ReceiveInformationRuralBehaviour extends OneShotBehaviour{
    @Override
    public void action(){
        CoordinatorAgent coordAgent = (CoordinatorAgent) myAgent;
        coordAgent.log("COMINH GERE********************************************************");
        // Find RAC and HC
        Map<AgentType, AID> coordinators = coordAgent.getCoordinators();
        AID ruralCoord = coordinators.get(AgentType.RURAL_AGENT_COORDINATOR);
        
        MessageTemplate mtrs = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
                                                   MessageTemplate.MatchSender(ruralCoord));
        ACLMessage savedReadyRural = myAgent.blockingReceive(mtrs);
        coordAgent.log("Received info about injured from RAC");
        
        
        LinkedList<Injured> savedByRural = new LinkedList<>();
        try {
            savedByRural = (LinkedList<Injured>) savedReadyRural.getContentObject();
            
        } catch (UnreadableException ex) {
            ex.printStackTrace();
        }
        Map<AgentType, LinkedList<Injured>> rescued = new HashMap<>();
        rescued.put(AgentType.RURAL_AGENT, savedByRural);
        
        ACLMessage injuredInfo = new ACLMessage(ACLMessage.INFORM);        
        injuredInfo.addReceiver(coordAgent.getCentralAgent());
        try {
            injuredInfo.setContentObject((Serializable) rescued);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        //injuredInfo.setPerformative(READY);
        coordAgent.send(injuredInfo);
        coordAgent.log("Content of data sent Info to Central: "+injuredInfo);
        coordAgent.log("Sending saved injureds to " + coordAgent.getCentralAgent());
        
        
        // CALL NEW BEHAVIOUR
        coordAgent.addBehaviour(new SendInformationRuralBehaviour());
        
    }
    
}
