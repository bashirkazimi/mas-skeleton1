/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.behaviour.coordinator;


import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.CoordinatorAgent;
import cat.urv.imas.onthology.GameSettings;
import cat.urv.imas.onthology.MessageContent;
import static cat.urv.imas.onthology.MessageContent.READY;
import cat.urv.imas.behaviour.coordinator.BuildGlobalPlanRuralCoord;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author andres
 */
public class SendInformationRuralBehaviour extends OneShotBehaviour{
    // This behaviour sends the injured people to the HC and RAC
    @Override
    public void action() {
        CoordinatorAgent coordAgent = (CoordinatorAgent) myAgent;

        MessageTemplate mtrs = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.REQUEST),
                                                  MessageTemplate.MatchSender(coordAgent.getCentralAgent()));
        ACLMessage activation = myAgent.blockingReceive(mtrs);
        
        try {
            GameSettings game = (GameSettings) activation.getContentObject();
            coordAgent.setGame(game);
        } catch (UnreadableException ex) {
            ex.printStackTrace();
        }
        // Find RAC and HC
        Map<AgentType, AID> coordinators = coordAgent.getCoordinators();
        AID ruralCoord = coordinators.get(AgentType.RURAL_AGENT_COORDINATOR);
        
        // Send to RAC
        ACLMessage gameMsg = new ACLMessage(ACLMessage.REQUEST);        
        
        
        gameMsg.addReceiver(ruralCoord);
        try {
            gameMsg.setContentObject(coordAgent.getGame());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        coordAgent.send(gameMsg);
        coordAgent.log("Sending Game to " + ruralCoord.getLocalName());
        
        
        // Now send info to 
        coordAgent.addBehaviour(new SendInformationBehaviour());

        //NEW BEHAVIOUR
        //coordAgent.addBehaviour(new BuildGlobalPlanRuralCoord());
        
    }
}