/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.behaviour.coordinator;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.agent.CoordinatorAgent;
import cat.urv.imas.onthology.Injured;
import cat.urv.imas.onthology.MessageContent;
import cat.urv.imas.behaviour.coordinator.ReceiveInformationRuralBehaviour;
import cat.urv.imas.utils.Pair;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 *
 * @author andres
 */
public class BuildGlobalPlanRuralCoord extends OneShotBehaviour {

    @Override
    public void action() {
        CoordinatorAgent coordAgent = (CoordinatorAgent) myAgent;
        coordAgent.log("------------IN BUILD GLOBAL PLAN---------------");
        // Find RAC and HC
        Map<AgentType, AID> coordinators = coordAgent.getCoordinators();
        AID ruralCoord = coordinators.get(AgentType.RURAL_AGENT_COORDINATOR);

        // Waiting for Helicopter coordinator to send Rural Partial Plan
        
        MessageTemplate mtrsRC = MessageTemplate.and(MessageTemplate.MatchPerformative(MessageContent.READY),
                                                  MessageTemplate.MatchSender(ruralCoord));        
        
        ACLMessage ruralPartialPlanMessage = myAgent.blockingReceive(mtrsRC);
        
        try{
        HashMap<AID,LinkedList<Pair>> objectRC = (HashMap<AID,LinkedList<Pair>>) ruralPartialPlanMessage.getContentObject();

        coordAgent.log("Message received from RAC with contracts from"+ruralPartialPlanMessage.getSender());
        coordAgent.log("Casted hashmap: "+objectRC.keySet());
                
        // Convert the hashmap to a hashmap with injured as keys
        
        Map<Integer, List<Pair>> injuredPartialPlan = new HashMap<>();
        Set<AID> aidset = objectRC.keySet();
        Iterator aidsetIter = aidset.iterator();
        if(aidsetIter.hasNext()){
                AID dummyAID = (AID)aidsetIter.next();
                List<Pair> listofPair = objectRC.get(dummyAID);
                 for(int i=0; i<listofPair.size();i++ ){
                        List<Pair> pairlist = new ArrayList<>();
                        Injured inj = (Injured)listofPair.get(i).getL();
                        coordAgent.log("Injured "+inj.id);
                        injuredPartialPlan.put(inj.id,pairlist);//.add(new Pair(RuralAID, listofPair.get(i).getR()));
                    }
                
        }
        
        coordAgent.log("INjured Partial Plan "+injuredPartialPlan.keySet().size());

        aidsetIter = aidset.iterator();
        while(aidsetIter.hasNext()){
           
           AID ruralAID = (AID)aidsetIter.next();
           List<Pair> listofPair = objectRC.get(ruralAID);
           for(int i=0; i<listofPair.size();i++ ){
               List<Pair> pairlist = new ArrayList<>();
               pairlist.add(new Pair(ruralAID, listofPair.get(i).getR()));
               Injured inj = (Injured)listofPair.get(i).getL();
//               if(i==0)
//                    injuredPartialPlan.put(inj, pairlist);
//               else
                    coordAgent.log("Injured here "+inj.id);
                   injuredPartialPlan.get(inj.id).add(new Pair(ruralAID, listofPair.get(i).getR()));
           }
        }
        coordAgent.log("Injured Partial Plan " + injuredPartialPlan.keySet());
        //ACLMessage.AGREE
        HashMap<AID, Integer> awardMap = new HashMap<>();
        List<AID> ruralAIDs = new LinkedList<>();
        int rnumber = coordAgent.getGame().getNumRuralAgents();
        coordAgent.log("HEREEEEEEEEEEE");
        // Find the helicopter AIDs
        for (int i=0; i<rnumber; i++){
            
            String name = "RuralAgent "+i;
            AID rural = new AID(name, AID.ISLOCALNAME);
            coordAgent.log(rural.getLocalName());
            ruralAIDs.add(rural);
        }
        
        // It breaks when we try to assign a rural and there is no 
        // next element in injuredPartialPlan
        // confirmed:
        // This happens when there are less injured people than rurals.
        // This will be fixed when we don't hardcode-test.
        
        coordAgent.log("Content of rural partial plan: " + injuredPartialPlan);
        Iterator it = injuredPartialPlan.keySet().iterator();
        for (AID ruralAID : ruralAIDs){
            awardMap.put(ruralAID, (Integer) it.next());
        }
        
        
        
        
        
        // We will compute a hashmap of AID -> task. Injured could be set to null if not selected or something.
        //Set<AID> helicopterAIDs = object.keySet();
        
        //HashMap<AID, Injured> awardMap = new HashMap<>();
        
        // Test sending back selected best bidder for each injured person
        // Get all the helicopter AIDs
        //Set<AID> aids = object.keySet();
        // THIS IS JUST FOR TEST. NEED TO COMPARE WITH LIST FROM RURALAGENTS
        // Prepare response to HAC
        
        ACLMessage reply = ruralPartialPlanMessage.createReply();
        reply.setPerformative(ACLMessage.CONFIRM);
        try{
            reply.setContentObject((Serializable)awardMap);
        
        //reply.setContentObject(awardMap);
        }catch(Exception e){
            coordAgent.log("Error sending the awards to RAC: ");
            e.printStackTrace();
        }
        
        // Send back the reply containing the awarded helicopters
        coordAgent.log("Sending award to RAC");
        coordAgent.send(reply);
        
        
        
        
        } catch(UnreadableException ex){
            ex.printStackTrace();
        }
        
        coordAgent.log("ABOUT TO CHANGE TO SEND INFORMATION FROM BUILD GLOBAL PLAN");

        myAgent.addBehaviour(new ReceiveInformationRuralBehaviour());
    }
    
}
