/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.onthology;

/**
 *
 * @author Bashir
 */
public class Injured implements java.io.Serializable{

     public int id;
     public int row;
     public int col;
     public int injuryType; //0 for light, 1 for heavy
     public int timeOfDeath;
     public boolean reachedHospital;
     public boolean pickedUp;
     public boolean assigned;

     public Injured(int id,int row, int col, int inj_type, int timeOfDeath, boolean reachedHospital){
        this.id = id;
        this.row = row;
        this.col = col;
        this.injuryType = inj_type;
        this.timeOfDeath = timeOfDeath;
        this.reachedHospital = reachedHospital;
        pickedUp = false;
        this.assigned = false;
    }
     @Override
    public boolean equals(Object arg) {
         if (!(arg instanceof Injured)){
            return false;
        }
        Injured I = (Injured)arg;
        if(I.id == this.id)
            return true;
        else
            return false;
    }

}
