
/**
 * IMAS base code for the practical work. Copyright (C) 2014 DEIM - URV
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.onthology;

import cat.urv.imas.agent.AgentType;
import cat.urv.imas.map.Cell;
import cat.urv.imas.map.CellType;
import cat.urv.imas.map.HospitalCell;
import cat.urv.imas.map.InjuredPeople;
import cat.urv.imas.map.MountainHutCell;
import cat.urv.imas.map.PathCell;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Current game settings. Cell coordinates are zero based: row and column values
 * goes from [0..n-1], both included.
 *
 * Use the GenerateGameSettings to build the game.settings configuration file.
 *
 */
@XmlRootElement(name = "GameSettings")
public class GameSettings implements java.io.Serializable {

    /* Default values set to all attributes, just in case. */
    /**
     * Seed for random numbers.
     */
    private float seed = 0.0f;
    /**
     * Capacity of helicopter, in number of people.
     */
    private int peoplePerHelicopter = 2;
    /**
     * Number of people loaded into a helicopter per simulation step.
     */
    private int loadingSpeed = 1;
    /**
     * Number of steps an avalanche can take long.
     */
    private int avalancheDuration = 5;
    /**
     * After this number of steps, injured people will die.
     */
    private int stepsToFreeze = 20;
    /**
     * This sets the light severity ratio of injured people. Grave severity will
     * be 100 - lightSeverity.
     */
    private int lightSeverity = 90;

    private int numInjuredCreated = 0;

    public int getNumInjuredCreated(){
        return this.numInjuredCreated;
    }
    public void incrementNumInjuredCreated(){
        this.numInjuredCreated++;
    }

    /**
     * Cost (think about money) for each person rescued by a rural agent.
     */
    private int ruralAgentCost = 1;
    /**
     * Cost (think about money) for each person rescued by a helicopter.
     */
    private int helicopterCost = 20;
    /**
     * Total number of simulation steps.
     */
    private int simulationSteps = 100;
    /**
     * City map.
     */
    private int maxAvalanche = 5;

    public int getMaxAvalanche(){
        return maxAvalanche;
    }

    private int numberofHelicopters = 0;
    private int numberofRuralAgents = 0;

    public void setNumHelicopters(int n){
        this.numberofHelicopters = n;
    }
    public void setNumRuralAgents(int n){
        this.numberofRuralAgents = n;
    }
    public int getNumHelicopters(){
        return numberofHelicopters;
    }
    public int getNumRuralAgents(){
        return numberofRuralAgents;
    }

    public int getTotalNumAgents(){
        return numberofRuralAgents+numberofHelicopters;
    }

    private int currentStep = 0;

    public int getCurrentStep(){
        return currentStep;
    }
    public void setCurrentStep(int s){
        currentStep = s;
    }
    public void incrementStep(){
        currentStep++;
    }
    protected Cell[][] map;
    /**
     * Computed summary of the position of agents in the city. For each given
     * type of mobile agent, we get the list of their positions.
     */
    protected Map<AgentType, List<Cell>> agentList;
    /**
     * Computed summary of the available list of injured people.
     */
    protected List<Injured> ListOfInjuredPeople = new ArrayList<>();


    /**
     * Title to set to the GUI.
     *
     *
     */
    protected String title = "Demo title";

    protected List<InjuredPeople> injuredPeople;

    public float getSeed() {
        return seed;
    }

    public boolean finished(){
        return currentStep > simulationSteps;
    }

    @XmlElement(required = true)
    public void setSeed(float seed) {
        this.seed = seed;
    }

    public int getSimulationSteps() {
        return simulationSteps;
    }

    @XmlElement(required = true)
    public void setSimulationSteps(int simulationSteps) {
        this.simulationSteps = simulationSteps;
    }

    public String getTitle() {
        return title;
    }

    @XmlElement(required = true)
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets the full current city map.
     *
     * @return the current city map.
     */
    @XmlTransient
    public Cell[][] getMap() {
        return map;
    }

    /**
     * Gets the cell given its coordinate.
     *
     * @param row row number (zero based)
     * @param col column number (zero based).
     * @return a city's Cell.
     */
    public Cell get(int row, int col) {
        return map[row][col];
    }

    public int getPeoplePerHelicopter() {
        return peoplePerHelicopter;
    }

    @XmlElement(required = true)
    public void setPeoplePerHelicopter(int peoplePerHelicopter) {
        this.peoplePerHelicopter = peoplePerHelicopter;
    }

    public int getLoadingSpeed() {
        return loadingSpeed;
    }

    @XmlElement(required = true)
    public void setLoadingSpeed(int loadingSpeed) {
        this.loadingSpeed = loadingSpeed;
    }

    public int getAvalancheDuration() {
        return avalancheDuration;
    }

    @XmlElement(required = true)
    public void setAvalancheDuration(int avalancheDuration) {
        this.avalancheDuration = avalancheDuration;
    }

    public int getStepsToFreeze() {
        return stepsToFreeze;
    }

    @XmlElement(required = true)
    public void setStepsToFreeze(int stepsToFreeze) {
        this.stepsToFreeze = stepsToFreeze;
    }

    public int getLightSeverity() {
        return lightSeverity;
    }

    @XmlElement(required = true)
    public void setLightSeverity(int lightSeverity) {
        this.lightSeverity = lightSeverity;
    }

    public int getRuralAgentCost() {
        return ruralAgentCost;
    }

    @XmlElement(required = true)
    public void setRuralAgentCost(int ruralAgentCost) {
        this.ruralAgentCost = ruralAgentCost;
    }

    public int getHelicopterCost() {
        return helicopterCost;
    }

    @XmlElement(required = true)
    public void setHelicopterCost(int helicopterCost) {
        this.helicopterCost = helicopterCost;
    }

    @XmlTransient
    public Map<AgentType, List<Cell>> getAgentList() {
        return agentList;
    }

    @XmlTransient
    public List<Injured> getInjuredPeopleList() {
        return ListOfInjuredPeople;
    }
    public void removeInjuredFromListCozItHasBeenTakenCareOf(Injured inj){
        ListOfInjuredPeople.remove(inj);
    }


    public void addInjuredPeople(Injured inj){
        ListOfInjuredPeople.add(inj);
        int row = inj.row;
        int col = inj.col;
        PathCell p = (PathCell)this.map[row][col];
        p.addOurInjuredPeople(inj);
    }

    public List<InjuredPeople> getInjuredPeople() {
        return injuredPeople;
    }

    public String toString() {
        //TODO: show a human readable summary of the game settings.
        return "Game settings";
    }






    public String getShortString() {
        //TODO: list of agents and other relevant settings.
        return "Game settings: agent related string";
    }


    public int cleanDeadPeopleAndAvalanches(){
        int num_dead = 0;
        System.out.println("    aasdf       "+map.length );
        for(int i=0; i<map.length; i++){
            for(int j=0; j<map[i].length; j++){
                if(map[i][j].getCellType() == CellType.PATH){
                    List<Injured> toRemove = new ArrayList<>();
                    PathCell p = (PathCell) map[i][j];
                    for(int k=0; k<p.getOurInjuredPeople().size();k++){
                        if(p.getOurInjuredPeople().get(k).timeOfDeath == this.currentStep){
                            toRemove.add(p.getOurInjuredPeople().get(k));
                        }
                    }
                    num_dead += toRemove.size();
                    p.removeDead(toRemove);

                    // clean avalanche;
                    if(p.isAvalanche() && p.getAvalancheSpan() == this.currentStep){
                        p.setAvalanche(false);
                    }
                }
            }
        }
        return num_dead;
    }


    public void createRandomInjured(int num){
        List<PathCell> pathList = new ArrayList<>();
        for(int i=0; i<map.length; i++){
            for(int j=0; j<map[i].length; j++){
                if(map[i][j].getCellType() == CellType.PATH ){
                    PathCell p = (PathCell) map[i][j];
                    if(!p.isAvalanche())
                        pathList.add((PathCell) map[i][j]);
                }
            }
        }
        float num_severedF = (((float) (100-this.lightSeverity)/ (float)100)) * (float) num ;
        float a = (100 - this.lightSeverity);
        float b = a/100;
        float c = b*num;
//        System.out.println("dd ");
//        System.out.println("ddddddd "+Math.ceil(c));
        int num_severed = (int) Math.ceil(c);
//        System.out.println("nummmmmmmmmmmmmmmmmmmmmmmmmmmmm "+num_severed);

        Random rand = new Random();
        int timeOfDeath = this.currentStep+this.stepsToFreeze;
        for(int i=0; i<num_severed; i++){
            int whichpathcell = rand.nextInt(pathList.size());
            int row = pathList.get(whichpathcell).getRow();
            int col = pathList.get(whichpathcell).getCol();
            Injured inj = new Injured(numInjuredCreated,row,col,1,timeOfDeath,false);
            numInjuredCreated++;
            addInjuredPeople(inj);
        }
        int num_light = num-num_severed;
        /*Injured inj = new Injured(numInjuredCreated,8,14,0,timeOfDeath,false);

           // Injured inj = new Injured(numInjuredCreated,row,col,0,timeOfDeath,false);
            numInjuredCreated++;
            addInjuredPeople(inj);
       Injured inj2 = new Injured(numInjuredCreated,1,5,0,timeOfDeath,false);

           // Injured inj = new Injured(numInjuredCreated,row,col,0,timeOfDeath,false);
            numInjuredCreated++;
            addInjuredPeople(inj2);

           Injured inj3 = new Injured(numInjuredCreated,12,14,0,timeOfDeath,false);

           // Injured inj = new Injured(numInjuredCreated,row,col,0,timeOfDeath,false);
            numInjuredCreated++;
            addInjuredPeople(inj3);

            Injured inj4 = new Injured(numInjuredCreated,1,14,0,timeOfDeath,false);

           // Injured inj = new Injured(numInjuredCreated,row,col,0,timeOfDeath,false);
            numInjuredCreated++;
            addInjuredPeople(inj4); */




        for(int i=0; i<num_light;i++){
            int whichpathcell = rand.nextInt(pathList.size());
            int row = pathList.get(whichpathcell).getRow();
            int col = pathList.get(whichpathcell).getCol();
                        Injured inj = new Injured(numInjuredCreated,row,col,0,timeOfDeath,false);

           // Injured inj = new Injured(numInjuredCreated,row,col,0,timeOfDeath,false);
            numInjuredCreated++;
            addInjuredPeople(inj);
        }
    }

    public List<HospitalCell> getHospitals(){
        List<HospitalCell> hospitals = new ArrayList<>();
        for(int i=0; i<map.length; i++){
            for(int j=0; j<map[i].length; j++){
                if(map[i][j].getCellType() == CellType.HOSPITAL){
                    HospitalCell h = (HospitalCell) map[i][j];
                    hospitals.add(h);
                }
            }
        }
        return hospitals;
    }

    public List<MountainHutCell> getMountainHuts(){
        List<MountainHutCell> mh = new ArrayList<>();
        for(int i=0; i<map.length; i++){
            for(int j=0; j<map[i].length; j++){
                if(map[i][j].getCellType() == CellType.MOUNTAIN_HUT){
                    MountainHutCell mhCell = (MountainHutCell) map[i][j];
                    mh.add(mhCell);
                }           
            }
        }
        return mh;
    }
    
//    public void createAvalanches(){
//        List<PathCell> pathList = new ArrayList<>();
//        for(int i=0; i<map.length; i++){
//            for(int j=0; j<map[i].length; j++){
//                if(map[i][j].getCellType() == CellType.PATH){
//                    pathList.add((PathCell) map[i][j]);
//                }
//            }
//        }
//        Random rand = new Random();
//        int numAvalancheThisStep = rand.nextInt(maxAvalanche);
//        for(int i=0; i<pathList.size();i++){
//            int whichCell = rand.nextInt(pathList.size());
//            if(pathList.get(whichCell).hasInjuredPeople()){
//
//                pathList.get(whichCell).removeDead(pathList.get(whichCell).getOurInjuredPeople());
//            }
//        }
//    }
}
