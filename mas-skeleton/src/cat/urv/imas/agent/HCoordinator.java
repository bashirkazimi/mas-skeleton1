/**
 *  IMAS base code for the practical work.
 *  Copyright (C) 2014 DEIM - URV
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.agent;

import cat.urv.imas.behaviour.helicoptercoordinator.ReceiveInjuredListBehaviour;
import cat.urv.imas.onthology.GameSettings;
import cat.urv.imas.onthology.Injured;
import cat.urv.imas.onthology.MessageContent;
import jade.core.*;
import jade.domain.*;
import jade.domain.FIPAAgentManagement.*;
import jade.domain.FIPANames.InteractionProtocol;
import jade.lang.acl.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * The Helicopter Coordinator . 
 * TODO: This Helicopter  coordinator should get info from the Coordinator
 * agent every round and give them to Helicopter agents to use them to save injured people.
 */
public class HCoordinator extends ImasAgent {

    /**
     * Game settings in use
     */
    private GameSettings game;
    /**
     * Coordinator agent id.
     */
    private AID coordinatorAgent;
    private AID myAID;
    private LinkedList<AID> HAList;
    private HashMap<Injured, HashMap<AID,Integer>> helicopterTotalBids;

    /**
     * Builds the Helicopter agent.
     */
    public HCoordinator() {
        super(AgentType.HELICOPTER_COORDINATOR);
        this.myAID = this.getAID();
    }

    public AID getMyAid(){
        return this.myAID;
    }
    /**
     * Agent setup method - called when it first come on-line. Configuration of
     * language to use, ontology and initialization of behaviours.
     */
    @Override
    protected void setup() {

        /* ** Very Important Line (VIL) ***************************************/
        this.setEnabledO2ACommunication(true, 1);
        /* ********************************************************************/

        // Register the agent to the DF
        ServiceDescription sd1 = new ServiceDescription();
        sd1.setType(AgentType.HELICOPTER_COORDINATOR.toString());
        sd1.setName(getLocalName());
        sd1.setOwnership(OWNER);
        
        // Create HAList
        HAList = new LinkedList<AID>();
        
        // Create HashMap that contains all the bids from all the helicopters
        HashMap<Injured, HashMap<AID,Integer>> helicopterTotalBids = new HashMap<>();

        
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.addServices(sd1);
        //dfd.setName(getAID());
        try {
            DFService.register(this, dfd);
            log("Registered to the DF");
        } catch (FIPAException e) {
            System.err.println(getLocalName() + " registration with DF unsucceeded. Reason: " + e.getMessage());
            doDelete();
        }
        Object [] args = getArguments();
        game = (GameSettings) args[0];
        // search CoordinatorAgent
        ServiceDescription searchCriterion = new ServiceDescription();
        searchCriterion.setType(AgentType.COORDINATOR.toString());
        this.coordinatorAgent = UtilsAgents.searchAgent(this, searchCriterion);
        // searchAgent is a blocking method, so we will obtain always a correct AID

        // Populate the list with the Helicopter Agents
        // ***Posibilidad de petar.**** ALERTA ALERTA
        int hnumber = this.getGame().getNumHelicopters();
        for (int i=0; i<hnumber; i++){
            
            String name = "Helicopter "+i;
            AID helicopter = new AID(name, AID.ISLOCALNAME);
            HAList.add(helicopter);
        }
        
        // Receive information from Coordinator Agent
        this.addBehaviour(new ReceiveInjuredListBehaviour());
        // setup finished. When we receive the last inform, the agent itself will add
        // a behaviour to send/receive actions
    }

    /**
     * Update the game settings.
     *
     * @param game current game settings.
     */
    public void setGame(GameSettings game) {
        this.game = game;
    }

    /**
     * Gets the current game settings.
     *
     * @return the current game settings.
     */
    public GameSettings getGame() {
        return this.game;
    }
    
    public LinkedList<AID> getHAList(){
        return this.HAList;
    }
    
    public HashMap getHelicopterTotalBids(){
        return this.helicopterTotalBids;
    }
    
    public void setHelicoptertotalBids(HashMap<Injured, HashMap<AID,Integer>> total){
        this.helicopterTotalBids = total;
    }

 
}
