/**
 * IMAS base code for the practical work.
 * Copyright (C) 2014 DEIM - URV
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.agent;

import cat.urv.imas.behaviour.central.CentralBehaviour;
import cat.urv.imas.behaviour.central.FillTheMapBehaviour;
import cat.urv.imas.onthology.InitialGameSettings;
import cat.urv.imas.onthology.GameSettings;
import cat.urv.imas.gui.GraphicInterface;
import cat.urv.imas.behaviour.central.RequestResponseBehaviour;
import cat.urv.imas.gui.Statistics;
import cat.urv.imas.map.Cell;
import cat.urv.imas.map.CellType;
import cat.urv.imas.map.PathCell;
import cat.urv.imas.onthology.InfoAgent;
import cat.urv.imas.utils.Positions;
import jade.core.*;
import static jade.core.MicroRuntime.getContainerName;
import jade.domain.*;
import jade.domain.FIPAAgentManagement.*;
import jade.domain.FIPANames.InteractionProtocol;
import jade.lang.acl.*;
import jade.core.Runtime;
import jade.wrapper.AgentContainer;
import jade.wrapper.ContainerController;
import jade.wrapper.ControllerException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Central agent that controls the GUI and loads initial configuration settings.
 * TODO: You have to decide the onthology and protocol when interacting among
 * the Coordinator agent.
 */
public class CentralAgent extends ImasAgent {

    /**
     * GUI with the map, central agent log and statistics.
     */
    private GraphicInterface gui;
    /**
     * Game settings. At the very beginning, it will contain the loaded initial
     * configuration settings.
     */
    private GameSettings game;
    /**
     * The Coordinator agent with which interacts sharing game settings every
     * round.
     */
    private AID coordinatorAgent;
    /**
     * Set of statistics to show up.
     */
    private Statistics statistics;

    /**
     * Builds the Central agent.
     */
    public CentralAgent() {
        super(AgentType.CENTRAL);
    }

    /**
     * A message is shown in the log area of the GUI, as well as in the stantard
     * output.
     *
     * @param log String to show
     */
    @Override
    public void log(String log) {
        if (gui != null) {
            gui.log(getLocalName() + ": " + log + "\n");
        }
        super.log(log);
    }

    /**
     * An error message is shown in the log area of the GUI, as well as in the
     * error output.
     *
     * @param error Error to show
     */
    @Override
    public void errorLog(String error) {
        if (gui != null) {
            gui.log("ERROR: " + getLocalName() + ": " + error + "\n");
        }
        super.errorLog(error);
    }

    /**
     * Gets the game settings.
     *
     * @return game settings.
     */
    public GameSettings getGame() {
        return this.game;
    }

    /**
     * Informs about the set of statistics.
     *
     * @return statistics.
     */
    public Statistics getStatistics() {
        return this.statistics;
    }
    
    public AID getCoordinatorAgent(){
        return this.coordinatorAgent;
    }

    /**
     * Agent setup method - called when it first come on-line. Configuration of
     * language to use, ontology and initialization of behaviours.
     */
    @Override
    protected void setup() {

        /* ** Very Important Line (VIL) ************************************* */
        this.setEnabledO2ACommunication(true, 1);

        // 1. Register the agent to the DF
        ServiceDescription sd1 = new ServiceDescription();
        sd1.setType(AgentType.CENTRAL.toString());
        sd1.setName(getLocalName());
        sd1.setOwnership(OWNER);

        DFAgentDescription dfd = new DFAgentDescription();
        dfd.addServices(sd1);
        dfd.setName(getAID());
        try {
            DFService.register(this, dfd);
            log("Registered to the DF");
        } catch (FIPAException e) {
            System.err.println(getLocalName() + " failed registration to DF [ko]. Reason: " + e.getMessage());
            doDelete();
        }

        // 2. Load game settings.
        this.game = InitialGameSettings.load("game.settings");
        log("Initial configuration settings loaded");
        this.statistics = new Statistics(this.game.getRuralAgentCost(), this.game.getHelicopterCost());
        
        // 3. Load GUI
        try {
            this.gui = new GraphicInterface(game);
            gui.showStatistics(statistics);
            gui.setVisible(true);
            log("GUI loaded");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // search CoordinatorAgent
        ServiceDescription searchCriterion = new ServiceDescription();
        searchCriterion.setType(AgentType.COORDINATOR.toString());
        
        this.coordinatorAgent = UtilsAgents.searchAgent(this, searchCriterion);
        // searchAgent is a blocking method, so we will obtain always a correct AID
   
        
        // Create the Helicopter Coordinator Agent;
        UtilsAgents.createAgent(this.getContainerController(), "HCoordinator" ,HCoordinator.class.getName() ,  new Object[]{this.game});
        
        // Create the Rural Agent Coordinator Agent;
        UtilsAgents.createAgent(this.getContainerController(), "RACoordinator" ,RACoordinator.class.getName() ,  new Object[]{this.game});
        
        System.out.println("???????????????????????????????????????????????????????");
        // ra keeps count of the rural agents in the gamesettings;
        int ra=0;
        
        /* iterate throught AgentList of the gamesettings to get cells in which the rural agent 
        / exist. then iterate through the number of agents in each cell
        */
        for(int i=0; i<this.game.getAgentList().get(AgentType.RURAL_AGENT).size();i++){
            for(int j=0; j<this.game.getAgentList().get(AgentType.RURAL_AGENT).get(i).getNumberOfAgents() ;j++){
                // spot the cell the agent found exists so we could pass as argument to createAgent function;
                Cell c = this.game.getAgentList().get(AgentType.RURAL_AGENT).get(i);
                // create agent with the name "RuralAgent "+ra which is count of agents to create distinct names
                // pass Cell c as argument to set which cell the agent exists;
                UtilsAgents.createAgent(this.getContainerController(), "RuralAgent "+Integer.toString(ra) ,RuralAgent.class.getName() , new Object[]{c, this.game});
                ra++;
            }
        }
        this.game.setNumRuralAgents(ra);

        // ha keeps count of the rural agents in the gamesettings;
        int ha=0;
        
        /* iterate throught AgentList of the gamesettings to get cells in which the helicopter agent 
        / exist. then iterate through the number of agents in each cell
        */
        for(int i=0; i<this.game.getAgentList().get(AgentType.HELICOPTER ).size();i++){
            for(int j=0; j<this.game.getAgentList().get(AgentType.HELICOPTER).get(i).getNumberOfAgents();j++ ){
                // spot the cell the agent found exists so we could pass as argument to createAgent function;
                Cell c = this.game.getAgentList().get(AgentType.HELICOPTER).get(i);
                // create agent with the name "Helicopter "+ha which is count of agents to create distinct names
                // pass Cell c as argument to set which cell the agent exists;                
                UtilsAgents.createAgent(this.getContainerController(), "Helicopter "+Integer.toString(ha) ,Helicopter.class.getName() ,  new Object[]{c,this.game});
                ha++;
            }
        }
        this.game.setNumHelicopters(ha);
        
        
        // add behaviours
        // we wait for the initialization of the game
        MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchProtocol(InteractionProtocol.FIPA_REQUEST), MessageTemplate.MatchPerformative(ACLMessage.REQUEST));
        this.addBehaviour(new RequestResponseBehaviour(this, mt));
        
        this.addBehaviour(new FillTheMapBehaviour());
    }

    public void updateGUI() {
        this.gui.showStatistics(statistics);
        this.gui.updateGame();
    }
    
    private List<Positions> getPathPositions() {
        Cell[][] map = this.game.getMap();
        int rows = map.length;
        int cols = map.length;
        List<Positions> pathCoords = new ArrayList<Positions>();
        Cell c;
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                c = map[row][col];
                if (c.getCellType() == CellType.PATH){
                    pathCoords.add(new Positions(row,col));
                }
            }
        }
        return pathCoords;
    }
}


