/**
 *  IMAS base code for the practical work.
 *  Copyright (C) 2014 DEIM - URV
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.agent;

import cat.urv.imas.behaviour.helicopter.WaitForAuctionBehaviour;
import cat.urv.imas.onthology.GameSettings;
import cat.urv.imas.map.Cell;
import cat.urv.imas.map.HospitalCell;
import cat.urv.imas.onthology.Injured;
import cat.urv.imas.onthology.MessageContent;
import jade.core.*;
import jade.domain.*;
import jade.domain.FIPAAgentManagement.*;
import jade.domain.FIPANames.InteractionProtocol;
import jade.lang.acl.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * The Helicopter agent. 
 * TODO: This helicopter should get info from the Coordinator
 * agent every round and  use them to save injured people.
 */
public class Helicopter extends ImasAgent {

    /**
     * Game settings in use
     */
    private GameSettings game;
    /**
     * Helicopter Coordinator agent id.
     */
    private AID HCoordinator;
    
    private List<Injured> pickedUp;
     /**
     * Current Cell the agent is in
     */
    private Cell currentCell;
    
    private Queue<Cell> path;
    
    private Cell targetCell;
    
    private boolean hospital = false;
    
    private Injured targetInjured;
    
    private Cell myhospital;

     /**
     * Aid of the agent
     */
    private AID aid;
            
    /**
     * Builds the rural agent coordinator agent.
     */
    public Helicopter() {
        super(AgentType.HELICOPTER);
    }

    /**
     * Agent setup method - called when it first come on-line. Configuration of
     * language to use, ontology and initialization of behaviours.
     */
    @Override
    protected void setup() {

        /* ** Very Important Line (VIL) ***************************************/
        this.setEnabledO2ACommunication(true, 1);
        /* ********************************************************************/

        // Register the agent to the DF
        ServiceDescription sd1 = new ServiceDescription();
        sd1.setType(AgentType.HELICOPTER.toString());
        sd1.setName(getLocalName());
        sd1.setOwnership(OWNER);
        
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.addServices(sd1);
        dfd.setName(getAID());
        try {
            DFService.register(this, dfd);
            log("Registered to the DF");
        } catch (FIPAException e) {
            System.err.println(getLocalName() + " registration with DF unsucceeded. Reason: " + e.getMessage());
            doDelete();
        }

        // Gets the current cell of the agent from the object parameter;
        Object [] args = getArguments();
        currentCell = (Cell) args[0];
        myhospital = new HospitalCell(currentCell.getRow(),currentCell.getCol());
        game = (GameSettings) args[1];
        pickedUp = new LinkedList<Injured>();
        path = new LinkedList<Cell>();
        // search CoordinatorAgent
        ServiceDescription searchCriterion = new ServiceDescription();
        searchCriterion.setType(AgentType.HELICOPTER_COORDINATOR.toString());
        this.HCoordinator = UtilsAgents.searchAgent(this, searchCriterion);
        // searchAgent is a blocking method, so we will obtain always a correct AID

        /* ********************************************************************/
        ACLMessage initialRequest = new ACLMessage(ACLMessage.REQUEST);
        initialRequest.clearAllReceiver();
        initialRequest.addReceiver(this.HCoordinator);
        initialRequest.setProtocol(InteractionProtocol.FIPA_REQUEST);
        log("Request message to agent");
        try {
            initialRequest.setContent("Send me some info, I'm a Helicopter");
            log("Request message content:" + initialRequest.getContent());
        } catch (Exception e) {
            e.printStackTrace();
        }

        //we add a behaviour that sends the message and waits for an answer
//        this.addBehaviour(new RequesterBehaviour(this, initialRequest));

        // setup finished. When we receive the last inform, the agent itself will add
        // a behaviour to send/receive actions
        
        // Wait for FPSB auction
        this.addBehaviour(new WaitForAuctionBehaviour());
    }

    public Cell getMyhospital() {
        return myhospital;
    }

    public void setMyhospital(Cell myhospital) {
        this.myhospital = myhospital;
    }

    /**
     *
     * @return aid of the agent;.
     */
    public AID getAgentAID(){
        return this.aid;
    }
    
    /**
     *
     * @return current cell of the agent;.
     */
    public Cell getCurrentCell(){
        return this.currentCell;
    }
    
    /**
     * Update the game settings.
     *
     * @param game current game settings.
     */
    public void setGame(GameSettings game) {
        this.game = game;
    }

    /**
     * Gets the current game settings.
     *
     * @return the current game settings.
     */
    public GameSettings getGame() {
        return this.game;
    }
    
    public AID getHCoordinator(){
        return this.HCoordinator;
    }
    
    public List<Injured> getPickedUp(){
        return this.pickedUp;
    }
    
    public void setPickedUp(Injured guy){
        this.pickedUp.add(guy);
    }

    public void setCurrentCell(Cell currentCell) {
        this.currentCell = currentCell;
    }

    public void setPath(Queue<Cell> path) {
        this.path = path;
    }

    public Queue<Cell> getPath() {
        return path;
    }

    public Cell getTargetCell() {
        return targetCell;
    }

    public void setTargetCell(Cell targetCell) {
        this.targetCell = targetCell;
    }
    
    public Injured getTargetInjured() {
        return targetInjured;
    }

    public void setTargetInjured(Injured targetInjured) {
        this.targetInjured = targetInjured;
    }

    public boolean isHospital() {
        return hospital;
    }

    public void setHospital(boolean hospital) {
        this.hospital = hospital;
    }
    
    
}
