/**
 *  IMAS base code for the practical work.
 *  Copyright (C) 2014 DEIM - URV
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.agent;

//import cat.urv.imas.behaviour.ruralAgent.WaitForContractNetBehaviour;
//import cat.urv.imas.behaviour.ruralAgent.WaitForContractNetBehaviour;
import cat.urv.imas.behaviour.ruralagent.RuralAgentContractNetResponderBehaviour;
import cat.urv.imas.onthology.GameSettings;
import cat.urv.imas.map.Cell;
import cat.urv.imas.onthology.MessageContent;
import jade.core.*;
import jade.domain.*;
import jade.domain.FIPAAgentManagement.*;
import jade.domain.FIPANames.InteractionProtocol;
import jade.lang.acl.*;
import cat.urv.imas.map.PathCell;
import cat.urv.imas.behaviour.ruralagent.WaitForContractNetBehaviour;
import cat.urv.imas.map.CellType;
import cat.urv.imas.map.HospitalCell;
import cat.urv.imas.map.MountainHutCell;
import cat.urv.imas.onthology.InfoAgent;
import cat.urv.imas.onthology.Injured;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * The Rural agent. 
 * TODO: This rural agent should get the game settings from the RACoordinator
 * agent every round and use them to move and save injured people.
 */
public class RuralAgent extends ImasAgent {

    /**
     * Game settings in use.
     */
    private GameSettings game;
    /**
     * RACoordinator agent id.
     */
    private AID RACoordinator;

    /**
     * current cell the agent is in;
     */
    private Cell currentCell; 
    
    private boolean comingBack;
    
    private Cell targetCell;
    
    private Injured targetInjured;
    
    private Cell myhut;
    
    private Injured pickedUp;
            

    /**
     * Builds the rural agent.
     */
    public RuralAgent() {
        super(AgentType.RURAL_AGENT);
    }

    /**
     * Agent setup method - called when it first come on-line. Configuration of
     * language to use, ontology and initialization of behaviours.
     */
    @Override
    protected void setup() {

        /* ** Very Important Line (VIL) ***************************************/
        this.setEnabledO2ACommunication(true, 1);
        /* ********************************************************************/

        // Register the agent to the DF
        ServiceDescription sd1 = new ServiceDescription();
        sd1.setType(AgentType.RURAL_AGENT.toString());
        sd1.setName(getLocalName());
        sd1.setOwnership(OWNER);
        
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.addServices(sd1);
        dfd.setName(getAID());
        try {
            DFService.register(this, dfd);
            log("Registered to the DF");
        } catch (FIPAException e) {
            System.err.println(getLocalName() + " registration with DF unsucceeded. Reason: " + e.getMessage());
            doDelete();
        }
        
        // Gets the current cell of the agent from the object parameter;
        Object [] args = getArguments();
        currentCell = (Cell) args[0];
        myhut = new MountainHutCell(currentCell.getRow(),currentCell.getCol());
        game = (GameSettings) args[1];
        path = new ArrayList<>();
        
        
        // search Rural Agent Coordinator
        ServiceDescription searchCriterion = new ServiceDescription();
        searchCriterion.setType(AgentType.RURAL_AGENT_COORDINATOR.toString());
        this.RACoordinator = UtilsAgents.searchAgent(this, searchCriterion);
        // searchAgent is a blocking method, so we will obtain always a correct AID

        /* ********************************************************************/
        ACLMessage initialRequest = new ACLMessage(ACLMessage.REQUEST);
        initialRequest.clearAllReceiver();
        initialRequest.addReceiver(this.RACoordinator);
        initialRequest.setProtocol(InteractionProtocol.FIPA_REQUEST);
        log("Request message to agent");
        try {
            initialRequest.setContent("Send me some info, I'm a rural agent");
            log("Request message content:" + initialRequest.getContent());
        } catch (Exception e) {
            e.printStackTrace();
        }

        //we add a behaviour that sends the message and waits for an answer
        //        this.addBehaviour(new WaitForContractNetBehaviour());
        this.addBehaviour(new RuralAgentContractNetResponderBehaviour());

        // setup finished. When we receive the last inform, the agent itself will add
         //a behaviour to send/receive actions
    }
    /**
     *
     * @return aid of the agent;.
     */
    public AID getAgentAID(){
        return this.aid;
    }
    // startCell --> cell where the rural agent is
    // endCell   --> cell where the injured person is
    // return: list with all the PathCell of the path, or NULL if there
    // no path
    
    public List<Cell> getshortestpath(Cell startCell, Cell endCell){
       
       Cell[][] map = game.getMap();
       int[][] levelMatrix = new int[map.length][map[0].length];
       for(int i=0; i<map.length; i++){
           for(int j=0; j<map[i].length; ++j)
           {
               CellType type = map[i][j].getCellType();
               if (type == CellType.HOSPITAL || type == CellType.MOUNTAIN ){
                   levelMatrix[i][j] = -1;
               }
               else{
                   levelMatrix[i][j] = 0;
               }
               // 0 FOR PATH, or FOR HUT CELL
               // -1 FOR MONTAIN AND HOSPITAL (NOT PATHCELL)
           }
       }
       LinkedList <Cell> queue = new LinkedList <Cell>();
       queue.add(startCell);
       levelMatrix[startCell.getRow()][startCell.getCol()] = 1;
       while (!queue.isEmpty()){
           Cell currentCell = queue.poll();   // remove the current cell from queue
           if (currentCell == endCell){
               break;
           }
           int row = currentCell.getRow();
           int col = currentCell.getCol();
           int level = levelMatrix[row][col];
           Cell[] nextCells = new Cell[4];
           nextCells[0] = map[row+1][col];
           nextCells[1] = map[row][col + 1];
           nextCells[2] = map[row - 1][col];
           nextCells[3] = map[row][col - 1];
           for (Cell nextCell : nextCells){
                int rowN = nextCell.getRow();
                int colN = nextCell.getCol();
                
                if (rowN < 0 || colN < 0){
                    continue;
                }
                if (rowN >= map.length || colN >= map[0].length){
                    continue;
                }
                if (levelMatrix[rowN][colN] == 0){
                    queue.add(nextCell);
                    levelMatrix[rowN][colN] = level + 1;
                }
           }
       }
       int eRow = endCell.getRow();
       int eCol = endCell.getCol();
       if (levelMatrix[eRow][eCol] == 0){
           return null;
       }
       LinkedList<Cell> shortestpath = new LinkedList<>();
       Cell current = endCell;
       while(!current.equals(startCell)){
           shortestpath.push(current);
           int row = current.getRow();
           int col = current.getCol();
           
           int level = levelMatrix[row][col];
           Cell[] nextCells = new Cell[4];
           nextCells[0] = map[row + 1][ col];
           nextCells[1] = map[row][col + 1];
           nextCells[2] = map[row - 1][col];
           nextCells[3] = map[row][col - 1];
           
           for (Cell nextCell : nextCells){
               int rowN = nextCell.getRow();
               int colN = nextCell.getCol();
               
                if (rowN < 0 || colN < 0)
                    continue;
                if (rowN >= map.length || colN >= map[0].length){
                    continue;
                }
                if (levelMatrix[rowN][colN] == level - 1){
                    current = nextCell;
                    break;
                }
                   
           }
       }
       return shortestpath;
    }
    
    /**
     *
     * @return current cell of the agent;.
     */
    public Cell getCurrentCell(){
        return this.currentCell;
    }
    
    /**
     * Update the game settings.
     *
     * @param game current game settings.
     */
    public void setGame(GameSettings game) {
        this.game = game;
    }

    
    /**
     * Gets the current game settings.
     *
     * @return the current game settings.
     */
    public GameSettings getGame() {
        return this.game;
    }
    
    public AID getRACoordinator(){
        return this.RACoordinator;
    }
    
    
    public List<Cell> getPath(){
        return this.path;
    }
    
    public void setPath(List<Cell> newPath){
        this.path = newPath;
    }
    public void computePath(int row, int col){
        PathCell target = (PathCell) this.getGame().getMap()[row][col];
        path = getshortestpath(currentCell, target);
        
    }
    
    public int distanceBetweenCells(int row,int col){
        PathCell target = (PathCell) this.getGame().getMap()[row][col];
        return getshortestpath(currentCell, target).size();
    }
    
    public Cell getFirstCellInPath(){
        if (this.getPath().isEmpty())
            return null;

        Cell toreturn = this.getPath().get(0);
        int row = toreturn.getRow();
        int col = toreturn.getCol();
        try{
            PathCell path = (PathCell) this.getGame().getMap()[row][col];
            if (path.isAvalanche()){
                return currentCell;
            }
        } catch(Exception e) {
        }
        this.getPath().remove(0);
        return toreturn;
    }
    
    public void moveToCell(Cell toMove){
            int nextrow = toMove.getRow();
            int nextcol = toMove.getCol();
            int actualrow = this.getCurrentCell().getRow();
            int actualcol = this.getCurrentCell().getCol();
            InfoAgent ra = new InfoAgent(AgentType.RURAL_AGENT,this.getAgentAID());
            
            // Move the agent in the game
            try {
                this.getGame().getMap()[actualrow][actualcol].removeAgent(ra);
                this.getGame().getMap()[nextrow][nextcol].addAgent(ra);
                this.setCurrentCell(toMove);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
    }
    
    public void setCurrentCell(Cell cur){
        this.currentCell = cur;
    }
    
    private List<Cell> path;
    /**
     * aid of the agent;.
     */
    private AID aid;

    public AID getAid() {
        return aid;
    }

    public void setAid(AID aid) {
        this.aid = aid;
    }

    public Injured getPickedUp() {
        return pickedUp;
    }

    public void setPickedUp(Injured pickedUp) {
        this.pickedUp = pickedUp;
    }
 
    public void setTargetCell(Cell t){
        this.targetCell = t;
    }
    public Cell getTargetCell(){
        return this.targetCell;
    }

    public boolean isComingBack() {
        return comingBack;
    }

    public void setComingBack(boolean comingBack) {
        this.comingBack = comingBack;
    }

    public Injured getTargetInjured() {
        return targetInjured;
    }

    public void setTargetInjured(Injured targetInjured) {
        this.targetInjured = targetInjured;
    }

    public Cell getMyhut() {
        return myhut;
    }

    public void setMyhut(Cell myhut) {
        this.myhut = myhut;
    }
    
    
    

}
