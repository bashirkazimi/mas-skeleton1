/**
 *  IMAS base code for the practical work.
 *  Copyright (C) 2014 DEIM - URV
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.urv.imas.agent;

import cat.urv.imas.onthology.GameSettings;
import cat.urv.imas.behaviour.ruralagentcoordinator.RequesterBehaviour;
import cat.urv.imas.behaviour.ruralagentcoordinator.ReceiveInjuredListBehaviour;
import cat.urv.imas.behaviour.ruralagentcoordinator.ResponderBehaviour;
import cat.urv.imas.onthology.MessageContent;
import jade.core.*;
import jade.domain.*;
import jade.domain.FIPAAgentManagement.*;
import jade.domain.FIPANames.InteractionProtocol;
import jade.lang.acl.*;
import java.util.LinkedList;

/**
 * The Rural Agent Coordinator agent. 
 * TODO: This rural agent coordinator should get info from the Coordinator
 * agent every round and give them to rural agents to use them to save injured people.
 */
public class RACoordinator extends ImasAgent {

    /**
     * Game settings in use
     */
    private GameSettings game;
    /**
     * Coordinator agent id.
     */
    private AID coordinatorAgent;
    private AID myAID;

    /**
     * Builds the rural agent coordinator agent.
     */
    private LinkedList<AID> RAList;
    
    public RACoordinator() {
        super(AgentType.RURAL_AGENT_COORDINATOR);
        this.myAID = this.getAID();
    }
    
    public AID getMyAid(){
        return this.myAID;
    }

    /**
     * Agent setup method - called when it first come on-line. Configuration of
     * language to use, ontology and initialization of behaviours.
     */
    @Override
    protected void setup() {

        /* ** Very Important Line (VIL) ***************************************/
        this.setEnabledO2ACommunication(true, 1);
        /* ********************************************************************/

        // Register the agent to the DF
        ServiceDescription sd1 = new ServiceDescription();
        sd1.setType(AgentType.RURAL_AGENT_COORDINATOR.toString());
        sd1.setName(getLocalName());
        sd1.setOwnership(OWNER);
        
        this.RAList = new LinkedList<AID>();
        
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.addServices(sd1);
        //dfd.setName(getAID());
        try {
            DFService.register(this, dfd);
            log("Registered to the DF");
        } catch (FIPAException e) {
            System.err.println(getLocalName() + " registration with DF unsucceeded. Reason: " + e.getMessage());
            doDelete();
        }
        
        Object [] args = getArguments();
        game = (GameSettings) args[0];
        // search CoordinatorAgent
        ServiceDescription searchCriterion = new ServiceDescription();
        searchCriterion.setType(AgentType.COORDINATOR.toString());
        this.coordinatorAgent = UtilsAgents.searchAgent(this, searchCriterion);
        // searchAgent is a blocking method, so we will obtain always a correct AID

        // PUEDE QUE PETE ESTO TAMBIEN ALERTA ALERTA
        
        
        int hnumber = this.getGame().getNumRuralAgents();
        
        this.log("RACCCCCCCCCCCCCCCCCCCCCC: "+this.getGame().getNumRuralAgents());
        for (int i=0; i<hnumber; i++){
            
            String name = "RuralAgent "+i;
            AID rural = new AID(name, AID.ISLOCALNAME);
            RAList.add(rural);
        }
        this.addBehaviour(new ReceiveInjuredListBehaviour());
        
        /* ********************************************************************/
        
        /*
                       CODIGO DE ANTES!!!!!!!!!!!!!!!!!!!!!!! 
        ACLMessage initialRequest = new ACLMessage(ACLMessage.REQUEST);
        initialRequest.clearAllReceiver();
        initialRequest.addReceiver(this.coordinatorAgent);
        initialRequest.setProtocol(InteractionProtocol.FIPA_REQUEST);
        log("Request message to agent");
        try {
            initialRequest.setContent("Send me some Info, I'm RACoordinator");
            log("Request message content:" + initialRequest.getContent());
        } catch (Exception e) {
            e.printStackTrace();
        }

        //we add a behaviour that sends the message and waits for an answer
        this.addBehaviour(new RequesterBehaviour(this, initialRequest));

        MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchProtocol(InteractionProtocol.FIPA_REQUEST), MessageTemplate.MatchPerformative(ACLMessage.REQUEST));

        this.addBehaviour(new ResponderBehaviour(this, mt));
        
        // setup finished. When we receive the last inform, the agent itself will add
        // a behaviour to send/receive actions   
        */
    }

    /**
     * Update the game settings.
     *
     * @param game current game settings.
     */
    public void setGame(GameSettings game) {
        this.game = game;
    }

    /**
     * Gets the current game settings.
     *
     * @return the current game settings.
     */
    public GameSettings getGame() {
        return this.game;
    }
    
    public LinkedList<AID> getRAList(){
        return this.RAList;
    }

}
