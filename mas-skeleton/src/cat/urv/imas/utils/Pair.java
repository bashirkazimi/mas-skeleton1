/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.imas.utils;

import java.io.Serializable;

/**
 *
 * @author juarugui
 */
public class Pair<L,R> implements Serializable {
    private L l;
    private R r;
    public Pair(L l, R r){
        this.l = l;
        this.r = r;
    }
    public L getL(){ return l; }
    public R getR(){ return r; }
    public void setL(L l){ this.l = l; }
    public void setR(R r){ this.r = r; }
    
    @Override
    public boolean equals(Object arg) {
         if (!(arg instanceof Pair)){
            return false;
        }
        Pair I = (Pair)arg;
        if(this.l == I.l)
            return true;
        else
            return false;
    }
    
    @Override
    public String toString(){
        return this.l.toString() +"/" +this.r.toString();
    }
}